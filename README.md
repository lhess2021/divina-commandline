# Divina Commandline

_Divina Commandline_ is a word search tool for the _Divine Comedy_. So far, it consists of several Bash scripts which parse cantos stored as text files. The cantos were pulled from https://digitaldante.columbia.edu/dante/divine-comedy/ by a Bash script which extracted them from the HTML source of the website.

For those who are not familiar, a [command line interface](https://en.wikipedia.org/wiki/Command-line_interface) processes commands for a programming language. I began this project with the goal of making it easy to interact with the _Comedy_ thought computer terminals, which function as a command line interface with the operating system. The name, "Divina Commandline" is thus a play on words--- a _condensation_ of "Divine Comedy" and "command line".

__Some scripts may currently be broken due to a new directory structure. They will be fixed soon.__

## Scripts

__`dd_scrape.sh`__
* for each canto, download the HTML source, use `sed` a bunch of times to remove the HTML

__`inf34_dd_scrape.sh`__
* repeat the above process for _Inferno 34_

### Word Search

__`words-sort-comedy.sh`__
* sort each canto by its words and remove duplicates -> text file `all-words-<canticle>-<number>.txt`
* apply a blacklist of unnecessary words -> text file `useful-words-<canticle>-<number>.txt`
* highlight the five most useful words in each canto to view in a file pager `highlighted-words-<canticle>-<number>.txt`

__`find-word.sh`__
* ask user for a word and `grep` the word in each `all-words-<canticle>-<number>.txt` file -> text file `num-<word>-per-canto.txt`
* sort this output to show which cantos have the most instances of the word -> text file `sorted-num-<word>-per-canto.txt`

__`count-total.sh`__
* ask user for a word and sum the counts from `num-<word>-per-canto.txt` to get a total count of the instances of the word in the _Comedy_

__`spreadsheet-num-words.sh`__
* get the information from `num-<word>-per-canto.txt` and format it into a `csv` file for use in a spreadsheet program
    * csv format: `Canto Number in <canticle>, Instances of <word>,, Canto Number in Comedy, Instances of <word>`


* * *

These scripts are not meant to be exact, polished, or well-commented. Please let me know of any issues.












