#!/bin/bash

# This script finds the words in all cantos of the Commedia
# and saves them all in the folders all-words/, useful-words/, and
# highlighted-words/.



clear
#mapfile -t BLACKLIST < "../blacklist"
#echo "$BLACKLIST"

CANTICLE="inferno"

for i in {1..34};
do
	echo "Processing $CANTICLE, Canto $i"
	cat "../$CANTICLE/$CANTICLE-$i" | tr " " "\n" | tr -d '[:punct:]|«|»' | sort -f | uniq -c | sort -nr > all-words/all-words-"$CANTICLE-$i".txt
	grep -vw -f ../blacklist all-words/all-words-"$CANTICLE-$i".txt > useful-words/useful-words-"$CANTICLE-$i".txt #inverse grep with respect to the full word
	GREP_PATT=$(echo $(head -n5 useful-words/useful-words-"$CANTICLE-$i".txt | awk '{print $2}') | sed 's/ / \\b\|\\b /g' | sed 's/^/\\b /' | sed 's/$/\\b\|/')
	grep --color=always -nE "$GREP_PATT" "../$CANTICLE/$CANTICLE-$i" > highlighted-words/highlighted-words-"$CANTICLE-$i".txt
done

CANTICLE="purgatorio"

for i in {1..33};
do
	echo "Processing $CANTICLE, Canto $i"
	cat "../$CANTICLE/$CANTICLE-$i" | tr " " "\n" | tr -d '[:punct:]' | sort -f | uniq -c | sort -nr > all-words/all-words-"$CANTICLE-$i".txt
	grep -vw -f ../blacklist all-words/all-words-"$CANTICLE-$i".txt > useful-words/useful-words-"$CANTICLE-$i".txt #inverse grep with respect to the full word
	GREP_PATT=$(echo $(head -n5 useful-words/useful-words-"$CANTICLE-$i".txt | awk '{print $2}') | sed 's/ / \\b\|\\b /g' | sed 's/^/\\b /' | sed 's/$/\\b\|/')
	grep --color=always -nE "$GREP_PATT" "../$CANTICLE/$CANTICLE-$i" > highlighted-words/highlighted-words-"$CANTICLE-$i".txt
done

CANTICLE="paradiso"

for i in {1..33};
do
	echo "Processing $CANTICLE, Canto $i"
	cat "../$CANTICLE/$CANTICLE-$i" | tr " " "\n" | tr -d '[:punct:]' | sort -f | uniq -c | sort -nr > all-words/all-words-"$CANTICLE-$i".txt
	grep -vw -f ../blacklist all-words/all-words-"$CANTICLE-$i".txt > useful-words/useful-words-"$CANTICLE-$i".txt #inverse grep with respect to the full word
	GREP_PATT=$(echo $(head -n5 useful-words/useful-words-"$CANTICLE-$i".txt | awk '{print $2}') | sed 's/ / \\b\|\\b /g' | sed 's/^/\\b /' | sed 's/$/\\b\|/')
	grep --color=always -nE "$GREP_PATT" "../$CANTICLE/$CANTICLE-$i" > highlighted-words/highlighted-words-"$CANTICLE-$i".txt
done
