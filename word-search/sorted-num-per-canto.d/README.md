# Sorted number of words per canto

This folder contains text files specific to a single word. Each file gives the number of occurrences of that word in each canto in a two-column format. The "occurrences" column is sorted from greatest to least, making it trivial to see which cantos have the most occurrences of that word.
