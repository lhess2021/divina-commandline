# Spreadsheets

This folder contains both csv and ods spreadsheets. The csv files are generated and the ods files are edited versions of the csv files, usually with graphs added.
