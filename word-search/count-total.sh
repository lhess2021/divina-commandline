#!/bin/bash

# This script counts the total occurences of a word in the Comedy

clear
read -p "Which word would you like to search for: " WORD
WL=${WORD,,}
WU=${WORD^}

canticles=( "inferno" "purgatorio" "paradiso" )

SUM=0
for CANTICLE in "${canticles[@]}";
do
	for i in {1..33};
	do
		echo "$CANTICLE-$i"
		COUNT_LOWER="$(grep -E "\b $WL$" all-words/all-words-$CANTICLE-$i.txt | awk '{print $1}')"
		[[ -z "$COUNT_LOWER" ]] && COUNT_LOWER=0
		COUNT_UPPER="$(grep -E "\b $WU$" all-words/all-words-$CANTICLE-$i.txt | awk '{print $1}')"
		[[ -z "$COUNT_UPPER" ]] && COUNT_UPPER=0
		# Ignore case
		COUNT_I=$(( "$COUNT_LOWER" + "$COUNT_UPPER" ))
		COUNT_I34=0
		if [[ $i = 33 ]];
		then
			if [[ "$CANTICLE" = "inferno" ]];
			then
				COUNT_LOWER="$(grep -E "\b $WL$" all-words/all-words-$CANTICLE-$34.txt | awk '{print $1}')"
				[[ -z "$COUNT_LOWER" ]] && COUNT_LOWER=0
				COUNT_UPPER="$(grep -E "\b $WU$" all-words/all-words-$CANTICLE-$34.txt | awk '{print $1}')"
				[[ -z "$COUNT_UPPER" ]] && COUNT_UPPER=0
				COUNT_I34=$(( "$COUNT_LOWER" + "$COUNT_UPPER" ))
			fi
		fi
		(( SUM += COUNT_I + COUNT_I34 ))
	done
done
echo "$SUM"
		# num-$WORD-per-canto.txt txt will have on each line:
		# 7 non inferno-x
		# 8 non inferno-y
		# 13 non inferno-z

#cat num-$WORD-per-canto.txt | sort -nr > num-$WORD-per-canto-sorted.txt

#clear
#echo -e "The file \`num-$WORD-per-canto.txt\` contains the number of occurrences of the word \`$WORD\` in each canto\n"
#echo -e "Here's the 20 cantos which have the most occurences of the word \`$WORD\`:\n"
#head -n20 num-$WORD-per-canto-sorted.txt
