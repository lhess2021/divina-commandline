#!/bin/bash

# This bash script asks for a word you want to find. Then it tells you the five cantos in which that word occurs the most frequently.


clear
read -p "Which word would you like to search for: " WORD
rm num-$WORD-per-canto.txt
rm sorted-num-$WORD-per-canto.txt

canticles=( "inferno" "purgatorio" "paradiso" )

for CANTICLE in "${canticles[@]}";
do
	for i in {1..33};
	do
		if [[ $i = 33 ]] && [[ "$CANTICLE" = "inferno" ]];
		then
			COUNT="$(grep -E "\b $WORD$" all-words/all-words-$CANTICLE-$i.txt | awk '{print $1}')"
			[[ -z "$COUNT" ]] && COUNT="0"
			OUTPUT="$COUNT $CANTICLE-$i"
			echo "$OUTPUT" >> .num-$WORD-per-canto.txt
			COUNT="$(grep -E "\b $WORD$" all-words/all-words-$CANTICLE-$i.txt | awk '{print $1}')"
			[[ -z "$COUNT" ]] && COUNT="0"
			OUTPUT="$COUNT $CANTICLE-$i"
			echo "$OUTPUT" >> .num-$WORD-per-canto.txt
		else
			COUNT="$(grep -E "\b $WORD$" all-words/all-words-$CANTICLE-$i.txt | awk '{print $1}')"
			[[ -z "$COUNT" ]] && COUNT="0"
			OUTPUT="$COUNT $CANTICLE-$i"
			echo "$OUTPUT" >> .num-$WORD-per-canto.txt
		fi
	done
done
		# num-$WORD-per-canto.txt txt will have on each line:
		# 7 non inferno-x
		# 8 non inferno-y
		# 13 non inferno-z

# Make it pretty and readable
# 23	Inf 1
# 7	Inf 2
cat .num-$WORD-per-canto.txt | column -t -o "     " | sed 's/inferno-/Inf. /g;s/purgatorio-/Pur. /g;s/paradiso-/Par. /g' > num-$WORD-per-canto.txt
rm .num-$WORD-per-canto.txt

cat num-$WORD-per-canto.txt | sort -nr > sorted-num-$WORD-per-canto.txt

clear
echo -e "The file \`num-$WORD-per-canto.txt\` contains the number of occurrences of the word \`$WORD\` in each canto\n"
echo -e "Here's the 20 cantos which have the most occurences of the word \`$WORD\`:\n"
head -n20 sorted-num-$WORD-per-canto.txt
