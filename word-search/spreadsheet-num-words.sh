#!/bin/bash

# This script takes a file of the form num-`word`-per-canto.txt
# And adds the data to a csv file in the form:
#
#       +--------------+---------+-----------------------+
#	|  Canticle    |  Canto  |  Instances of `word`  |
#	+--------------+---------+-----------------------+
#       |  Inferno     |    1    |           11          |
#	|              |    2    |           19          |
#	|              |   ...   |           ..          |
#	|  Paradiso    |   100   |            4          |
#	+--------------+---------+-----------------------+

# The canticle formatting has not yet been implemented.

clear
read -p "Which word do you want to make a csv of: " WORD
rm spreadsheets.d/csv-$WORD.csv

echo "Canto Number in Canticle, Instances of \"$WORD\",,Canto Number in Comedy, Instances of \"$WORD\"" >> csv-$WORD.csv

cat num-per-canto.d/num-$WORD-per-canto.txt | while read line
do
	# Make each canto out of 100 for graphing purposes
	if [[ $(echo $line | awk '{print $2}') = "Inf." ]];
	then
		CANTO=$(echo $line | awk '{print $3}')
		NUM_WORD=$(echo $line | awk '{print $1}')
		CSV_INPUT=$(echo "$CANTO","$NUM_WORD",,"$CANTO","$NUM_WORD")
		echo "$CSV_INPUT" >> spreadsheets.d/csv-$WORD.csv
	elif [[ $(echo $line | awk '{print $2}') = "Pur." ]];
	then
		CANTO=$(echo $line | awk '{print $3}')
		CANTO_PLUS=$(( $(echo $line | awk '{print $3}') + 34 ))
		NUM_WORD=$(echo $line | awk '{print $1}')
		CSV_INPUT=$(echo "$CANTO","$NUM_WORD",,"$CANTO_PLUS","$NUM_WORD")
		echo "$CSV_INPUT" >> spreadsheets.d/csv-$WORD.csv
	else
		CANTO=$(echo $line | awk '{print $3}')
		CANTO_PLUS=$(( $(echo $line | awk '{print $3}') + 67 ))
		NUM_WORD=$(echo $line | awk '{print $1}')
		CSV_INPUT=$(echo "$CANTO","$NUM_WORD",,"$CANTO_PLUS","$NUM_WORD")
		echo "$CSV_INPUT" >> spreadsheets.d/csv-$WORD.csv
	fi
done
