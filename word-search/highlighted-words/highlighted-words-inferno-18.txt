[32m[K1[m[K[36m[K:[m[KLuogo è in inferno detto Malebolge,
[32m[K2[m[K[36m[K:[m[Ktutto di pietra di color ferrigno,
[32m[K3[m[K[36m[K:[m[Kcome la cerchia che dintorno il volge.
[32m[K4[m[K[36m[K:[m[KNel dritto mezzo del campo maligno
[32m[K5[m[K[36m[K:[m[Kvaneggia un pozzo assai largo e profondo,
[32m[K6[m[K[36m[K:[m[Kdi cui suo loco dicerò l’ordigno.
[32m[K7[m[K[36m[K:[m[KQuel cinghio che rimane adunque è tondo
[32m[K8[m[K[36m[K:[m[Ktra ’l pozzo e ’l piè de l’alta ripa dura,
[32m[K9[m[K[36m[K:[m[Ke ha distinto in dieci valli il fondo.
[32m[K10[m[K[36m[K:[m[KQuale, dove per guardia de le mura
[32m[K11[m[K[36m[K:[m[Kpiù e[01;31m[K più [m[Kfossi cingon li castelli,
[32m[K12[m[K[36m[K:[m[Kla parte dove[01;31m[K son [m[Krende figura,
[32m[K13[m[K[36m[K:[m[Ktale imagine quivi facean quelli;
[32m[K14[m[K[36m[K:[m[Ke come a tai fortezze da’ lor sogli
[32m[K15[m[K[36m[K:[m[Ka la ripa di fuor[01;31m[K son [m[Kponticelli,
[32m[K16[m[K[36m[K:[m[Kcosì da imo de la roccia scogli
[32m[K17[m[K[36m[K:[m[Kmovien che ricidien li argini e ’ fossi
[32m[K18[m[K[36m[K:[m[Kinfino al pozzo che i tronca e raccogli.
[32m[K19[m[K[36m[K:[m[KIn questo luogo, de la schiena scossi
[32m[K20[m[K[36m[K:[m[Kdi Gerïon, trovammoci; e ’l poeta
[32m[K21[m[K[36m[K:[m[Ktenne a sinistra, e[01;31m[K io [m[Kdietro mi mossi.
[32m[K22[m[K[36m[K:[m[KA la man destra vidi nova pieta,
[32m[K23[m[K[36m[K:[m[Knovo tormento e novi frustatori,
[32m[K24[m[K[36m[K:[m[Kdi che la prima bolgia era repleta.
[32m[K25[m[K[36m[K:[m[KNel fondo erano ignudi i peccatori;
[32m[K26[m[K[36m[K:[m[Kdal mezzo in qua ci venien verso ’l volto,
[32m[K27[m[K[36m[K:[m[Kdi là con noi, ma con passi maggiori,
[32m[K28[m[K[36m[K:[m[Kcome i Roman per l’essercito molto,
[32m[K29[m[K[36m[K:[m[Kl’anno del giubileo, su per lo ponte
[32m[K30[m[K[36m[K:[m[Khanno a passar la gente modo colto,
[32m[K31[m[K[36m[K:[m[Kche da l’un lato tutti hanno la fronte
[32m[K32[m[K[36m[K:[m[Kverso ’l castello e vanno a Santo Pietro,
[32m[K33[m[K[36m[K:[m[Kda l’altra sponda vanno verso ’l monte.
[32m[K34[m[K[36m[K:[m[KDi qua, di là, su per lo sasso tetro
[32m[K35[m[K[36m[K:[m[Kvidi demon cornuti con gran ferze,
[32m[K36[m[K[36m[K:[m[Kche li battien crudelmente di retro.
[32m[K37[m[K[36m[K:[m[KAhi come facean lor levar le berze
[32m[K38[m[K[36m[K:[m[Ka le prime percosse! già nessuno
[32m[K39[m[K[36m[K:[m[Kle seconde aspettava né le terze.
[32m[K40[m[K[36m[K:[m[KMentr’ io andava, li occhi miei in uno
[32m[K41[m[K[36m[K:[m[Kfuro scontrati; e[01;31m[K io [m[Ksì tosto dissi:
[32m[K42[m[K[36m[K:[m[K«Già di veder costui[01;31m[K non [m[Kson digiuno».
[32m[K43[m[K[36m[K:[m[KPer ch’ïo a figurarlo i piedi affissi;
[32m[K44[m[K[36m[K:[m[Ke ’l dolce duca meco si ristette,
[32m[K45[m[K[36m[K:[m[Ke assentio ch’alquanto in dietro gissi.
[32m[K46[m[K[36m[K:[m[KE quel frustato celar si credette
[32m[K47[m[K[36m[K:[m[Kbassando ’l viso; ma poco li valse,
[32m[K48[m[K[36m[K:[m[Kch’io dissi: «O tu che l’occhio a terra gette,
[32m[K49[m[K[36m[K:[m[Kse le fazion che porti[01;31m[K non [m[Kson false,
[32m[K50[m[K[36m[K:[m[KVenedico se’ tu Caccianemico.
[32m[K51[m[K[36m[K:[m[KMa che ti mena a sì pungenti salse?».
[32m[K52[m[K[36m[K:[m[KEd elli a me: «Mal volentier lo dico;
[32m[K53[m[K[36m[K:[m[Kma sforzami la tua chiara favella,
[32m[K54[m[K[36m[K:[m[Kche mi fa sovvenir del mondo antico.
[32m[K55[m[K[36m[K:[m[KI’ fui colui che la Ghisolabella
[32m[K56[m[K[36m[K:[m[Kcondussi a far la voglia del marchese,
[32m[K57[m[K[36m[K:[m[Kcome che suoni la sconcia novella.
[32m[K58[m[K[36m[K:[m[KE[01;31m[K non [m[Kpur[01;31m[K io [m[Kqui piango bolognese;
[32m[K59[m[K[36m[K:[m[Kanzi n’è questo luogo tanto pieno,
[32m[K60[m[K[36m[K:[m[Kche tante lingue[01;31m[K non [m[Kson ora apprese
[32m[K61[m[K[36m[K:[m[Ka dicer ‘sipa’ tra Sàvena e Reno;
[32m[K62[m[K[36m[K:[m[Ke se di ciò vuoi fede o testimonio,
[32m[K63[m[K[36m[K:[m[Krècati a mente il nostro avaro seno».
[32m[K64[m[K[36m[K:[m[KCosì parlando il percosse un demonio
[32m[K65[m[K[36m[K:[m[Kde la sua scurïada, e[01;31m[K disse[m[K: «Via,
[32m[K66[m[K[36m[K:[m[Kruffian! qui[01;31m[K non [m[Kson femmine da conio».
[32m[K67[m[K[36m[K:[m[KI’ mi raggiunsi con la scorta mia;
[32m[K68[m[K[36m[K:[m[Kposcia con pochi passi divenimmo
[32m[K69[m[K[36m[K:[m[Klà ’v’ uno scoglio de la ripa uscia.
[32m[K70[m[K[36m[K:[m[KAssai leggeramente quel salimmo;
[32m[K71[m[K[36m[K:[m[Ke vòlti a destra su per la sua scheggia,
[32m[K72[m[K[36m[K:[m[Kda quelle cerchie etterne ci partimmo.
[32m[K73[m[K[36m[K:[m[KQuando noi fummo là dov’ el vaneggia
[32m[K74[m[K[36m[K:[m[Kdi sotto per dar passo a li sferzati,
[32m[K75[m[K[36m[K:[m[Klo duca[01;31m[K disse[m[K: «Attienti, e fa che feggia
[32m[K76[m[K[36m[K:[m[Klo viso in te di quest’ altri mal nati,
[32m[K77[m[K[36m[K:[m[Kai quali ancor[01;31m[K non [m[Kvedesti la faccia
[32m[K78[m[K[36m[K:[m[Kperò che[01;31m[K son [m[Kcon noi insieme andati».
[32m[K79[m[K[36m[K:[m[KDel vecchio ponte guardavam la traccia
[32m[K80[m[K[36m[K:[m[Kche venìa verso noi da l’ altra banda,
[32m[K81[m[K[36m[K:[m[Ke che la ferza similmente scaccia.
[32m[K82[m[K[36m[K:[m[KE ’l buon maestro, sanza mia dimanda,
[32m[K83[m[K[36m[K:[m[Kmi[01;31m[K disse[m[K: «Guarda quel grande che vene,
[32m[K84[m[K[36m[K:[m[Ke per dolor[01;31m[K non [m[Kpar lagrime spanda:
[32m[K85[m[K[36m[K:[m[Kquanto aspetto reale ancor ritene!
[32m[K86[m[K[36m[K:[m[KQuelli è Iasón, che per cuore e per senno
[32m[K87[m[K[36m[K:[m[Kli Colchi del monton privati féne.
[32m[K88[m[K[36m[K:[m[KEllo passò per l’isola di Lenno,
[32m[K89[m[K[36m[K:[m[Kpoi che l’ardite femmine spietate
[32m[K90[m[K[36m[K:[m[Ktutti li maschi loro a morte dienno.
[32m[K91[m[K[36m[K:[m[KIvi con segni e con parole ornate
[32m[K92[m[K[36m[K:[m[KIsifile ingannò, la giovinetta
[32m[K93[m[K[36m[K:[m[Kche prima avea tutte l’ altre ingannate.
[32m[K94[m[K[36m[K:[m[KLasciolla quivi, gravida, soletta;
[32m[K95[m[K[36m[K:[m[Ktal colpa a tal martiro lui condanna;
[32m[K96[m[K[36m[K:[m[Ke anche di Medea si fa vendetta.
[32m[K97[m[K[36m[K:[m[KCon lui sen va chi da tal parte inganna:
[32m[K98[m[K[36m[K:[m[Ke questo basti de la prima valle
[32m[K99[m[K[36m[K:[m[Ksapere e di color che ’n sé assanna».
[32m[K100[m[K[36m[K:[m[KGià eravam là ’ve lo stretto calle
[32m[K101[m[K[36m[K:[m[Kcon l’ argine secondo s’incrocicchia,
[32m[K102[m[K[36m[K:[m[Ke fa di quello ad un altr’ arco spalle.
[32m[K103[m[K[36m[K:[m[KQuindi sentimmo gente che si nicchia
[32m[K104[m[K[36m[K:[m[Kne l’altra bolgia e che col muso scuffa,
[32m[K105[m[K[36m[K:[m[Ke sé medesma con le palme picchia.
[32m[K106[m[K[36m[K:[m[KLe ripe eran grommate d’una muffa,
[32m[K107[m[K[36m[K:[m[Kper l’alito di giù che vi s’appasta,
[32m[K108[m[K[36m[K:[m[Kche con li occhi e col naso facea zuffa.
[32m[K109[m[K[36m[K:[m[KLo fondo è cupo sì, che[01;31m[K non [m[Kci basta
[32m[K110[m[K[36m[K:[m[Kloco a veder sanza montare al dosso
[32m[K111[m[K[36m[K:[m[Kde l’arco, ove lo scoglio[01;31m[K più [m[Ksovrasta.
[32m[K112[m[K[36m[K:[m[KQuivi venimmo; e quindi giù nel fosso
[32m[K113[m[K[36m[K:[m[Kvidi gente attuffata in uno sterco
[32m[K114[m[K[36m[K:[m[Kche da li uman privadi parea mosso.
[32m[K115[m[K[36m[K:[m[KE mentre ch’io là giù con l’occhio cerco,
[32m[K116[m[K[36m[K:[m[Kvidi un col capo sì di merda lordo,
[32m[K117[m[K[36m[K:[m[Kche[01;31m[K non [m[Kparëa s’era laico o cherco.
[32m[K118[m[K[36m[K:[m[KQuei mi sgridò: «Perché se’ tu sì gordo
[32m[K119[m[K[36m[K:[m[Kdi riguardar[01;31m[K più [m[Kme che li altri brutti?».
[32m[K120[m[K[36m[K:[m[KE[01;31m[K io [m[Ka lui: «Perché, se ben ricordo,
[32m[K121[m[K[36m[K:[m[Kgià t’ho veduto coi capelli asciutti,
[32m[K122[m[K[36m[K:[m[Ke se’ Alessio Interminei da Lucca:
[32m[K123[m[K[36m[K:[m[Kperò t’adocchio[01;31m[K più [m[Kche li altri tutti».
[32m[K124[m[K[36m[K:[m[KEd elli allor, battendosi la zucca:
[32m[K125[m[K[36m[K:[m[K«Qua giù m’hanno sommerso le lusinghe
[32m[K126[m[K[36m[K:[m[Kond’io[01;31m[K non [m[Kebbi mai la lingua stucca».
[32m[K127[m[K[36m[K:[m[KAppresso ciò lo duca «Fa che pinghe»,
[32m[K128[m[K[36m[K:[m[Kmi[01;31m[K disse[m[K «il viso un poco[01;31m[K più [m[Kavante,
[32m[K129[m[K[36m[K:[m[Ksì che la faccia ben con l’occhio attinghe
[32m[K130[m[K[36m[K:[m[Kdi quella sozza e scapigliata fante
[32m[K131[m[K[36m[K:[m[Kche là si graffia con l’unghie merdose,
[32m[K132[m[K[36m[K:[m[Ke or s’accoscia e ora è in piedi stante.
[32m[K133[m[K[36m[K:[m[KTaïde è, la puttana che rispuose
[32m[K134[m[K[36m[K:[m[Kal drudo suo quando[01;31m[K disse[m[K “Ho[01;31m[K io [m[Kgrazie
[32m[K135[m[K[36m[K:[m[Kgrandi apo te?”: “Anzi maravigliose!”.
[32m[K136[m[K[36m[K:[m[KE quinci sien le nostre viste sazie».
