[32m[K1[m[K[36m[K:[m[KParea dinanzi a me con l’ali aperte
[32m[K2[m[K[36m[K:[m[Kla bella image che nel dolce frui
[32m[K3[m[K[36m[K:[m[Kliete facevan l’anime conserte;
[32m[K4[m[K[36m[K:[m[Kparea ciascuna rubinetto in cui
[32m[K5[m[K[36m[K:[m[Kraggio di sole ardesse sì acceso,
[32m[K6[m[K[36m[K:[m[Kche ne’ miei occhi rifrangesse lui.
[32m[K7[m[K[36m[K:[m[KE quel che mi convien ritrar testeso,
[32m[K8[m[K[36m[K:[m[Knon portò voce mai, né scrisse incostro,
[32m[K9[m[K[36m[K:[m[Kné fu per fantasia già[01;31m[K mai [m[Kcompreso;
[32m[K10[m[K[36m[K:[m[Kch’io vidi e anche udi’ parlar lo rostro,
[32m[K11[m[K[36m[K:[m[Ke sonar ne la voce e «io» e «mio»,
[32m[K12[m[K[36m[K:[m[Kquand’ era nel concetto e ‘noi’ e ‘nostro’.
[32m[K13[m[K[36m[K:[m[KE cominciò: «Per esser giusto e pio
[32m[K14[m[K[36m[K:[m[Kson io qui essaltato a quella gloria
[32m[K15[m[K[36m[K:[m[Kche[01;31m[K non [m[Ksi lascia vincere a disio;
[32m[K16[m[K[36m[K:[m[Ke in terra lasciai la mia memoria
[32m[K17[m[K[36m[K:[m[Ksì fatta, che le genti lì malvage
[32m[K18[m[K[36m[K:[m[Kcommendan lei, ma[01;31m[K non [m[Kseguon la storia».
[32m[K19[m[K[36m[K:[m[KCosì un sol calor di molte brage
[32m[K20[m[K[36m[K:[m[Ksi fa sentir, come di molti amori
[32m[K21[m[K[36m[K:[m[Kusciva solo un suon di quella image.
[32m[K22[m[K[36m[K:[m[KOnd’ io appresso: «O perpetüi fiori
[32m[K23[m[K[36m[K:[m[Kde l’etterna letizia, che pur uno
[32m[K24[m[K[36m[K:[m[Kparer mi fate tutti vostri odori,
[32m[K25[m[K[36m[K:[m[Ksolvetemi, spirando, il gran digiuno
[32m[K26[m[K[36m[K:[m[Kche lungamente m’ha tenuto in fame,
[32m[K27[m[K[36m[K:[m[Knon trovandoli in terra cibo alcuno.
[32m[K28[m[K[36m[K:[m[KBen so io che, se ’n cielo altro reame
[32m[K29[m[K[36m[K:[m[Kla divina giustizia fa suo specchio,
[32m[K30[m[K[36m[K:[m[Kche ’l vostro[01;31m[K non [m[Kl’apprende con velame.
[32m[K31[m[K[36m[K:[m[KSapete come attento io m’apparecchio
[32m[K32[m[K[36m[K:[m[Kad ascoltar; sapete qual è quello
[32m[K33[m[K[36m[K:[m[Kdubbio che m’è digiun cotanto vecchio».
[32m[K34[m[K[36m[K:[m[KQuasi falcone ch’esce del cappello,
[32m[K35[m[K[36m[K:[m[Kmove la testa e con l’ali si plaude,
[32m[K36[m[K[36m[K:[m[Kvoglia mostrando e faccendosi bello,
[32m[K37[m[K[36m[K:[m[Kvid’ io farsi quel segno, che di laude
[32m[K38[m[K[36m[K:[m[Kde la divina grazia era contesto,
[32m[K39[m[K[36m[K:[m[Kcon canti quai si sa[01;31m[K chi[m[K là sù gaude.
[32m[K40[m[K[36m[K:[m[KPoi cominciò: «Colui che volse il sesto
[32m[K41[m[K[36m[K:[m[Ka lo stremo del mondo, e dentro ad esso
[32m[K42[m[K[36m[K:[m[Kdistinse tanto occulto e manifesto,
[32m[K43[m[K[36m[K:[m[Knon poté suo valor sì fare impresso
[32m[K44[m[K[36m[K:[m[Kin tutto l’universo, che ’l suo verbo
[32m[K45[m[K[36m[K:[m[Knon rimanesse in infinito eccesso.
[32m[K46[m[K[36m[K:[m[KE ciò fa certo che ’l primo superbo,
[32m[K47[m[K[36m[K:[m[Kche fu la somma d’ogne creatura,
[32m[K48[m[K[36m[K:[m[Kper[01;31m[K non [m[Kaspettar lume, cadde acerbo;
[32m[K49[m[K[36m[K:[m[Ke quinci appar ch’ogne minor natura
[32m[K50[m[K[36m[K:[m[Kè corto recettacolo a quel bene
[32m[K51[m[K[36m[K:[m[Kche[01;31m[K non [m[Kha fine e sé con sé misura.
[32m[K52[m[K[36m[K:[m[KDunque vostra veduta, che convene
[32m[K53[m[K[36m[K:[m[Kesser alcun de’ raggi de la mente
[32m[K54[m[K[36m[K:[m[Kdi che tutte le cose son ripiene,
[32m[K55[m[K[36m[K:[m[Knon pò da sua natura esser possente
[32m[K56[m[K[36m[K:[m[Ktanto, che suo principio discerna
[32m[K57[m[K[36m[K:[m[Kmolto di là da quel che l’è parvente.
[32m[K58[m[K[36m[K:[m[KPerò ne la giustizia sempiterna
[32m[K59[m[K[36m[K:[m[Kla vista che riceve il vostro mondo,
[32m[K60[m[K[36m[K:[m[Kcom’ occhio per lo mare, entro s’interna;
[32m[K61[m[K[36m[K:[m[Kche, ben che da la proda veggia il fondo,
[32m[K62[m[K[36m[K:[m[Kin pelago nol vede; e nondimeno
[32m[K63[m[K[36m[K:[m[Kèli, ma cela lui l’esser profondo.
[32m[K64[m[K[36m[K:[m[KLume[01;31m[K non [m[Kè, se[01;31m[K non [m[Kvien dal sereno
[32m[K65[m[K[36m[K:[m[Kche[01;31m[K non [m[Ksi turba mai; anzi è tenèbra
[32m[K66[m[K[36m[K:[m[Kod ombra de la carne o suo veleno.
[32m[K67[m[K[36m[K:[m[KAssai t’è mo aperta la latebra
[32m[K68[m[K[36m[K:[m[Kche t’ascondeva la giustizia viva,
[32m[K69[m[K[36m[K:[m[Kdi che facei question cotanto crebra;
[32m[K70[m[K[36m[K:[m[Kché tu dicevi: “Un uom nasce a la riva
[32m[K71[m[K[36m[K:[m[Kde l’Indo, e quivi[01;31m[K non [m[Kè[01;31m[K chi[m[K ragioni
[32m[K72[m[K[36m[K:[m[Kdi Cristo[01;31m[K né [m[Kchi legga[01;31m[K né [m[Kchi scriva;
[32m[K73[m[K[36m[K:[m[Ke tutti suoi voleri e atti buoni
[32m[K74[m[K[36m[K:[m[Ksono, quanto ragione umana vede,
[32m[K75[m[K[36m[K:[m[Ksanza peccato in vita o in sermoni.
[32m[K76[m[K[36m[K:[m[KMuore[01;31m[K non [m[Kbattezzato e sanza fede:
[32m[K77[m[K[36m[K:[m[Kov’ è questa giustizia che ’l condanna?
[32m[K78[m[K[36m[K:[m[Kov’ è la colpa sua, se ei[01;31m[K non [m[Kcrede?”.
[32m[K79[m[K[36m[K:[m[KOr tu[01;31m[K chi[m[K se’, che vuo’ sedere a scranna,
[32m[K80[m[K[36m[K:[m[Kper giudicar di lungi mille miglia
[32m[K81[m[K[36m[K:[m[Kcon la veduta corta d’una spanna?
[32m[K82[m[K[36m[K:[m[KCerto a colui che meco s’assottiglia,
[32m[K83[m[K[36m[K:[m[Kse la Scrittura sovra voi[01;31m[K non [m[Kfosse,
[32m[K84[m[K[36m[K:[m[Kda dubitar sarebbe a maraviglia.
[32m[K85[m[K[36m[K:[m[KOh terreni animali! oh menti grosse!
[32m[K86[m[K[36m[K:[m[KLa prima volontà, ch’è da sé buona,
[32m[K87[m[K[36m[K:[m[Kda sé, ch’è sommo ben, mai[01;31m[K non [m[Ksi mosse.
[32m[K88[m[K[36m[K:[m[KCotanto è giusto quanto a lei consuona:
[32m[K89[m[K[36m[K:[m[Knullo creato bene a sé la tira,
[32m[K90[m[K[36m[K:[m[Kma essa, radïando, lui cagiona».
[32m[K91[m[K[36m[K:[m[KQuale sovresso il nido si rigira
[32m[K92[m[K[36m[K:[m[Kpoi c’ha pasciuti la cicogna i figli,
[32m[K93[m[K[36m[K:[m[Ke come quel ch’è pasto la rimira;
[32m[K94[m[K[36m[K:[m[Kcotal si fece, e sì leväi i cigli,
[32m[K95[m[K[36m[K:[m[Kla benedetta imagine, che l’ali
[32m[K96[m[K[36m[K:[m[Kmovea sospinte da tanti consigli.
[32m[K97[m[K[36m[K:[m[KRoteando cantava, e dicea: «Quali
[32m[K98[m[K[36m[K:[m[Kson le mie note a te, che[01;31m[K non [m[Kle ’ntendi,
[32m[K99[m[K[36m[K:[m[Ktal è il giudicio etterno a voi mortali».
[32m[K100[m[K[36m[K:[m[KPoi si quetaro quei lucenti incendi
[32m[K101[m[K[36m[K:[m[Kde lo Spirito Santo ancor nel segno
[32m[K102[m[K[36m[K:[m[Kche fé i Romani al mondo reverendi,
[32m[K103[m[K[36m[K:[m[Kesso ricominciò: «A questo regno
[32m[K104[m[K[36m[K:[m[Knon salì[01;31m[K mai [m[Kchi[01;31m[K non [m[Kcredette ’n Cristo,
[32m[K105[m[K[36m[K:[m[Kné pria[01;31m[K né [m[Kpoi ch’el si chiavasse al legno.
[32m[K106[m[K[36m[K:[m[KMa vedi: molti gridan “Cristo, Cristo!”,
[32m[K107[m[K[36m[K:[m[Kche saranno in giudicio assai men prope
[32m[K108[m[K[36m[K:[m[Ka lui, che tal che[01;31m[K non [m[Kconosce Cristo;
[32m[K109[m[K[36m[K:[m[Ke tai Cristian dannerà l’Etïòpe,
[32m[K110[m[K[36m[K:[m[Kquando si partiranno i due collegi,
[32m[K111[m[K[36m[K:[m[Kl’uno in etterno ricco e l’altro inòpe.
[32m[K112[m[K[36m[K:[m[KChe poran dir li Perse a’ vostri regi,
[32m[K113[m[K[36m[K:[m[Kcome vedranno quel volume aperto
[32m[K114[m[K[36m[K:[m[Knel qual si scrivon tutti suoi dispregi?
[32m[K115[m[K[36m[K:[m[KLì si vedrà, tra l’opere d’Alberto,
[32m[K116[m[K[36m[K:[m[Kquella che tosto moverà la penna,
[32m[K117[m[K[36m[K:[m[Kper che ’l regno di Praga fia diserto.
[32m[K118[m[K[36m[K:[m[KLì si vedrà il duol che sovra Senna
[32m[K119[m[K[36m[K:[m[Kinduce, falseggiando la moneta,
[32m[K120[m[K[36m[K:[m[Kquel che morrà di colpo di cotenna.
[32m[K121[m[K[36m[K:[m[KLì si vedrà la superbia ch’asseta,
[32m[K122[m[K[36m[K:[m[Kche fa lo Scotto e l’Inghilese folle,
[32m[K123[m[K[36m[K:[m[Ksì che[01;31m[K non [m[Kpuò soffrir dentro a sua meta.
[32m[K124[m[K[36m[K:[m[KVedrassi la lussuria e ’l viver molle
[32m[K125[m[K[36m[K:[m[Kdi quel di Spagna e di quel di Boemme,
[32m[K126[m[K[36m[K:[m[Kche[01;31m[K mai [m[Kvalor[01;31m[K non [m[Kconobbe[01;31m[K né [m[Kvolle.
[32m[K127[m[K[36m[K:[m[KVedrassi al Ciotto di Ierusalemme
[32m[K128[m[K[36m[K:[m[Ksegnata con un i la sua bontate,
[32m[K129[m[K[36m[K:[m[Kquando ’l contrario segnerà un emme.
[32m[K130[m[K[36m[K:[m[KVedrassi l’avarizia e la viltate
[32m[K131[m[K[36m[K:[m[Kdi quei che guarda l’isola del foco,
[32m[K132[m[K[36m[K:[m[Kove Anchise finì la lunga etate;
[32m[K133[m[K[36m[K:[m[Ke a dare ad intender quanto è poco,
[32m[K134[m[K[36m[K:[m[Kla sua scrittura fian lettere mozze,
[32m[K135[m[K[36m[K:[m[Kche noteranno molto in parvo loco.
[32m[K136[m[K[36m[K:[m[KE parranno a ciascun l’opere sozze
[32m[K137[m[K[36m[K:[m[Kdel barba e del fratel, che tanto egregia
[32m[K138[m[K[36m[K:[m[Knazione e due corone han fatte bozze.
[32m[K139[m[K[36m[K:[m[KE quel di Portogallo e di Norvegia
[32m[K140[m[K[36m[K:[m[Klì si conosceranno, e quel di Rascia
[32m[K141[m[K[36m[K:[m[Kche male ha visto il conio di Vinegia.
[32m[K142[m[K[36m[K:[m[KOh beata Ungheria, se[01;31m[K non [m[Ksi lascia
[32m[K143[m[K[36m[K:[m[Kpiù malmenare! e beata Navarra,
[32m[K144[m[K[36m[K:[m[Kse s’armasse del monte che la fascia!
[32m[K145[m[K[36m[K:[m[KE creder de’ ciascun che già, per arra
[32m[K146[m[K[36m[K:[m[Kdi questo, Niccosïa e Famagosta
[32m[K147[m[K[36m[K:[m[Kper la lor bestia si lamenti e garra,
[32m[K148[m[K[36m[K:[m[Kche dal fianco de l’altre[01;31m[K non [m[Ksi scosta».
