[32m[K1[m[K[36m[K:[m[KQuando per dilettanze o ver per doglie,
[32m[K2[m[K[36m[K:[m[Kche alcuna virtù nostra comprenda,
[32m[K3[m[K[36m[K:[m[Kl’anima bene ad essa si raccoglie,
[32m[K4[m[K[36m[K:[m[Kpar ch’a nulla potenza più intenda;
[32m[K5[m[K[36m[K:[m[Ke questo è contra quello error che crede
[32m[K6[m[K[36m[K:[m[Kch’un’anima sovr’ altra in[01;31m[K noi [m[Ks’accenda.
[32m[K7[m[K[36m[K:[m[KE però, quando s’ode cosa o vede
[32m[K8[m[K[36m[K:[m[Kche tegna forte a sé l’anima volta,
[32m[K9[m[K[36m[K:[m[Kvassene ’l tempo e l’uom[01;31m[K non [m[Kse n’avvede;
[32m[K10[m[K[36m[K:[m[Kch’altra potenza è quella che l’ascolta,
[32m[K11[m[K[36m[K:[m[Ke altra è quella c’ha l’anima intera:
[32m[K12[m[K[36m[K:[m[Kquesta è quasi legata e quella è sciolta.
[32m[K13[m[K[36m[K:[m[KDi ciò ebb’ io esperïenza vera,
[32m[K14[m[K[36m[K:[m[Kudendo quello spirto e ammirando;
[32m[K15[m[K[36m[K:[m[Kché ben cinquanta gradi salito era
[32m[K16[m[K[36m[K:[m[Klo sole, e[01;31m[K io [m[Knon m’era accorto, quando
[32m[K17[m[K[36m[K:[m[Kvenimmo ove quell’anime ad una
[32m[K18[m[K[36m[K:[m[Kgridaro a noi: «Qui è vostro dimando».
[32m[K19[m[K[36m[K:[m[KMaggiore aperta molte volte impruna
[32m[K20[m[K[36m[K:[m[Kcon una forcatella di sue spine
[32m[K21[m[K[36m[K:[m[Kl’uom de la villa quando l’uva imbruna,
[32m[K22[m[K[36m[K:[m[Kche[01;31m[K non [m[Kera la calla onde salìne
[32m[K23[m[K[36m[K:[m[Klo duca mio, e[01;31m[K io [m[Kappresso, soli,
[32m[K24[m[K[36m[K:[m[Kcome da[01;31m[K noi [m[Kla schiera si partìne.
[32m[K25[m[K[36m[K:[m[KVassi in Sanleo e discendesi in Noli,
[32m[K26[m[K[36m[K:[m[Kmontasi su in Bismantova e ’n Cacume
[32m[K27[m[K[36m[K:[m[Kcon esso i piè; ma qui convien ch’ om voli;
[32m[K28[m[K[36m[K:[m[Kdico con l’ale snelle e con le piume
[32m[K29[m[K[36m[K:[m[Kdel gran disio, di retro a quel condotto
[32m[K30[m[K[36m[K:[m[Kche speranza mi dava e facea lume.
[32m[K31[m[K[36m[K:[m[KNoi salavam per entro ’l sasso rotto,
[32m[K32[m[K[36m[K:[m[Ke d’ogne lato ne stringea lo stremo,
[32m[K33[m[K[36m[K:[m[Ke piedi e man volea il suol di sotto.
[32m[K34[m[K[36m[K:[m[KPoi che[01;31m[K noi [m[Kfummo in su l’orlo suppremo
[32m[K35[m[K[36m[K:[m[Kde l’alta ripa, a la scoperta piaggia,
[32m[K36[m[K[36m[K:[m[K«Maestro mio», diss’ io, «che via faremo?».
[32m[K37[m[K[36m[K:[m[KEd elli a me: «Nessun tuo passo caggia;
[32m[K38[m[K[36m[K:[m[Kpur su al monte dietro a me acquista,
[32m[K39[m[K[36m[K:[m[Kfin che n’appaia alcuna scorta saggia».
[32m[K40[m[K[36m[K:[m[KLo sommo er’ alto che vincea la vista,
[32m[K41[m[K[36m[K:[m[Ke la costa superba più assai
[32m[K42[m[K[36m[K:[m[Kche da mezzo quadrante a centro lista.
[32m[K43[m[K[36m[K:[m[KIo era lasso, quando cominciai:
[32m[K44[m[K[36m[K:[m[K«O dolce padre, volgiti, e rimira
[32m[K45[m[K[36m[K:[m[Kcom’ io rimango sol, se[01;31m[K non [m[Krestai».
[32m[K46[m[K[36m[K:[m[K«Figliuol mio», disse, «infin quivi ti tira»,
[32m[K47[m[K[36m[K:[m[Kadditandomi un balzo poco in sùe
[32m[K48[m[K[36m[K:[m[Kche da quel lato il poggio tutto gira.
[32m[K49[m[K[36m[K:[m[KSì mi spronaron le parole sue,
[32m[K50[m[K[36m[K:[m[Kch’i’ mi sforzai carpando appresso lui,
[32m[K51[m[K[36m[K:[m[Ktanto che ’l cinghio sotto i piè mi fue.
[32m[K52[m[K[36m[K:[m[KA seder ci ponemmo ivi ambedui
[32m[K53[m[K[36m[K:[m[Kvòlti a levante ond’ eravam saliti,
[32m[K54[m[K[36m[K:[m[Kche suole a riguardar giovare altrui.
[32m[K55[m[K[36m[K:[m[KLi occhi prima drizzai ai bassi liti;
[32m[K56[m[K[36m[K:[m[Kposcia li alzai al sole, e ammirava
[32m[K57[m[K[36m[K:[m[Kche da sinistra n’eravam feriti.
[32m[K58[m[K[36m[K:[m[KBen s’avvide il poeta ch’ïo stava
[32m[K59[m[K[36m[K:[m[Kstupido tutto al carro de la luce,
[32m[K60[m[K[36m[K:[m[Kove tra[01;31m[K noi [m[Ke Aquilone intrava.
[32m[K61[m[K[36m[K:[m[KOnd’ elli a me: «Se Castore e Poluce
[32m[K62[m[K[36m[K:[m[Kfossero in compagnia di quello specchio
[32m[K63[m[K[36m[K:[m[Kche[01;31m[K sù[m[K e giù del suo lume conduce,
[32m[K64[m[K[36m[K:[m[Ktu vedresti il Zodïaco rubecchio
[32m[K65[m[K[36m[K:[m[Kancora a l’Orse più stretto rotare,
[32m[K66[m[K[36m[K:[m[Kse[01;31m[K non [m[Kuscisse fuor del cammin vecchio.
[32m[K67[m[K[36m[K:[m[KCome ciò sia, se ’l vuoi poter pensare,
[32m[K68[m[K[36m[K:[m[Kdentro raccolto, imagina Sïòn
[32m[K69[m[K[36m[K:[m[Kcon questo monte in su la terra stare
[32m[K70[m[K[36m[K:[m[Ksì, ch’amendue hanno un solo orizzòn
[32m[K71[m[K[36m[K:[m[Ke diversi emisperi; onde la strada
[32m[K72[m[K[36m[K:[m[Kche mal[01;31m[K non [m[Kseppe carreggiar Fetòn,
[32m[K73[m[K[36m[K:[m[Kvedrai come a costui convien che vada
[32m[K74[m[K[36m[K:[m[Kda l’un, quando a colui da l’altro fianco,
[32m[K75[m[K[36m[K:[m[Kse lo ’ntelletto tuo ben chiaro bada».
[32m[K76[m[K[36m[K:[m[K«Certo, maestro mio,» diss’ io, «unquanco
[32m[K77[m[K[36m[K:[m[Knon vid’ io chiaro sì com’ io discerno
[32m[K78[m[K[36m[K:[m[Klà dove mio ingegno parea manco,
[32m[K79[m[K[36m[K:[m[Kche ’l mezzo cerchio del moto superno,
[32m[K80[m[K[36m[K:[m[Kche si chiama Equatore in alcun’ arte,
[32m[K81[m[K[36m[K:[m[Ke che sempre riman tra ’l sole e ’l verno,
[32m[K82[m[K[36m[K:[m[Kper la ragion che di’, quinci si parte
[32m[K83[m[K[36m[K:[m[Kverso settentrïon, quanto li Ebrei
[32m[K84[m[K[36m[K:[m[Kvedevan lui verso la calda parte.
[32m[K85[m[K[36m[K:[m[KMa[01;31m[K se [m[Ka te piace, volontier saprei
[32m[K86[m[K[36m[K:[m[Kquanto avemo ad andar; ché ’l poggio sale
[32m[K87[m[K[36m[K:[m[Kpiù che salir[01;31m[K non [m[Kposson li occhi miei».
[32m[K88[m[K[36m[K:[m[KEd elli a me: «Questa montagna è tale,
[32m[K89[m[K[36m[K:[m[Kche sempre al cominciar di sotto è grave;
[32m[K90[m[K[36m[K:[m[Ke quant’ om più va[01;31m[K sù[m[K, e men fa male.
[32m[K91[m[K[36m[K:[m[KPerò, quand’ ella ti parrà soave
[32m[K92[m[K[36m[K:[m[Ktanto, che[01;31m[K sù[m[K andar ti fia leggero
[32m[K93[m[K[36m[K:[m[Kcom’ a seconda giù andar per nave,
[32m[K94[m[K[36m[K:[m[Kallor sarai al fin d’esto sentiero;
[32m[K95[m[K[36m[K:[m[Kquivi di riposar l’affanno aspetta.
[32m[K96[m[K[36m[K:[m[KPiù[01;31m[K non [m[Krispondo, e questo so per vero».
[32m[K97[m[K[36m[K:[m[KE com’ elli ebbe sua parola detta,
[32m[K98[m[K[36m[K:[m[Kuna voce di presso sonò: «Forse
[32m[K99[m[K[36m[K:[m[Kche di sedere in pria avrai distretta!».
[32m[K100[m[K[36m[K:[m[KAl suon di lei ciascun di[01;31m[K noi [m[Ksi torse,
[32m[K101[m[K[36m[K:[m[Ke vedemmo a mancina un gran petrone,
[32m[K102[m[K[36m[K:[m[Kdel qual né[01;31m[K io [m[Kné ei prima s’accorse.
[32m[K103[m[K[36m[K:[m[KLà ci traemmo; e ivi eran persone
[32m[K104[m[K[36m[K:[m[Kche si stavano a l’ombra dietro al sasso
[32m[K105[m[K[36m[K:[m[Kcome l’uom per negghienza a star si pone.
[32m[K106[m[K[36m[K:[m[KE un di lor, che mi sembiava lasso,
[32m[K107[m[K[36m[K:[m[Ksedeva e abbracciava le ginocchia,
[32m[K108[m[K[36m[K:[m[Ktenendo ’l viso giù tra esse basso.
[32m[K109[m[K[36m[K:[m[K«O dolce segnor mio», diss’ io, «adocchia
[32m[K110[m[K[36m[K:[m[Kcolui che mostra sé più negligente
[32m[K111[m[K[36m[K:[m[Kche[01;31m[K se [m[Kpigrizia fosse sua serocchia».
[32m[K112[m[K[36m[K:[m[KAllor si volse a[01;31m[K noi [m[Ke puose mente,
[32m[K113[m[K[36m[K:[m[Kmovendo ’l viso pur su per la coscia,
[32m[K114[m[K[36m[K:[m[Ke disse: «Or va tu[01;31m[K sù[m[K, che se’ valente!».
[32m[K115[m[K[36m[K:[m[KConobbi allor chi era, e quella angoscia
[32m[K116[m[K[36m[K:[m[Kche m’avacciava un poco ancor la lena,
[32m[K117[m[K[36m[K:[m[Knon m’impedì l’andare a lui; e poscia
[32m[K118[m[K[36m[K:[m[Kch’a lui fu’ giunto, alzò la testa a pena,
[32m[K119[m[K[36m[K:[m[Kdicendo: «Hai ben veduto come ’l sole
[32m[K120[m[K[36m[K:[m[Kda l’omero sinistro il carro mena?».
[32m[K121[m[K[36m[K:[m[KLi atti suoi pigri e le corte parole
[32m[K122[m[K[36m[K:[m[Kmosser le labbra mie un poco a riso;
[32m[K123[m[K[36m[K:[m[Kpoi cominciai: «Belacqua, a me[01;31m[K non [m[Kdole
[32m[K124[m[K[36m[K:[m[Kdi te omai; ma dimmi: perché assiso
[32m[K125[m[K[36m[K:[m[Kquiritto se’? attendi tu iscorta,
[32m[K126[m[K[36m[K:[m[Ko pur lo modo usato t’ha’ ripriso?».
[32m[K127[m[K[36m[K:[m[KEd elli: «O frate, andar in[01;31m[K sù[m[K che porta?
[32m[K128[m[K[36m[K:[m[Kché[01;31m[K non [m[Kmi lascerebbe ire a’ martìri
[32m[K129[m[K[36m[K:[m[Kl’angel di Dio che siede in su la porta.
[32m[K130[m[K[36m[K:[m[KPrima convien che tanto il ciel m’ aggiri
[32m[K131[m[K[36m[K:[m[Kdi fuor da essa, quanto fece in vita,
[32m[K132[m[K[36m[K:[m[Kperch’ io ’ndugiai al fine i buon sospiri,
[32m[K133[m[K[36m[K:[m[Kse orazïone in prima[01;31m[K non [m[Km’aita
[32m[K134[m[K[36m[K:[m[Kche surga[01;31m[K sù[m[K di cuor che in grazia viva;
[32m[K135[m[K[36m[K:[m[Kl’altra che val, che ’n ciel[01;31m[K non [m[Kè udita?».
[32m[K136[m[K[36m[K:[m[KE già il poeta innanzi mi saliva,
[32m[K137[m[K[36m[K:[m[Ke dicea: «Vienne omai; vedi ch’è tocco
[32m[K138[m[K[36m[K:[m[Kmeridïan dal sole e a la riva
[32m[K139[m[K[36m[K:[m[Kcuopre la notte già col piè Morrocco».
