[32m[K1[m[K[36m[K:[m[K﻿Ricorditi, lettor, se mai[01;31m[K ne [m[Kl’alpe
[32m[K2[m[K[36m[K:[m[Kti colse nebbia per la qual vedessi
[32m[K3[m[K[36m[K:[m[Knon altrimenti che per pelle talpe,
[32m[K4[m[K[36m[K:[m[Kcome, quando i vapori umidi e spessi
[32m[K5[m[K[36m[K:[m[Ka diradar cominciansi, la spera
[32m[K6[m[K[36m[K:[m[Kdel sol debilemente entra per essi;
[32m[K7[m[K[36m[K:[m[Ke fia la tua imagine leggera
[32m[K8[m[K[36m[K:[m[Kin giugnere a veder com’ io rividi
[32m[K9[m[K[36m[K:[m[Klo sole in pria, che già nel corcar era.
[32m[K10[m[K[36m[K:[m[KSì, pareggiando i miei co’ passi fidi
[32m[K11[m[K[36m[K:[m[Kdel mio maestro, usci’ fuor di tal nube
[32m[K12[m[K[36m[K:[m[Kai raggi morti già ne’ bassi lidi.
[32m[K13[m[K[36m[K:[m[KO imaginativa che[01;31m[K ne [m[Krube
[32m[K14[m[K[36m[K:[m[Ktalvolta sì di fuor, ch’om[01;31m[K non [m[Ks’accorge
[32m[K15[m[K[36m[K:[m[Kperché dintorno suonin mille tube,
[32m[K16[m[K[36m[K:[m[Kchi move te, se ’l senso[01;31m[K non [m[Kti porge?
[32m[K17[m[K[36m[K:[m[KMoveti lume che nel ciel s’informa,
[32m[K18[m[K[36m[K:[m[Kper[01;31m[K sé [m[Ko per voler che giù lo scorge.
[32m[K19[m[K[36m[K:[m[KDe l’empiezza di lei che mutò forma
[32m[K20[m[K[36m[K:[m[Kne l’uccel ch’a cantar più si diletta,
[32m[K21[m[K[36m[K:[m[Kne l’imagine[01;31m[K mia[m[K apparve l’orma;
[32m[K22[m[K[36m[K:[m[Ke qui fu la[01;31m[K mia[m[K mente sì ristretta
[32m[K23[m[K[36m[K:[m[Kdentro da sé, che di fuor[01;31m[K non [m[Kvenìa
[32m[K24[m[K[36m[K:[m[Kcosa che fosse allor da lei ricetta.
[32m[K25[m[K[36m[K:[m[KPoi piovve dentro a l’alta fantasia
[32m[K26[m[K[36m[K:[m[Kun crucifisso, dispettoso e fero
[32m[K27[m[K[36m[K:[m[Kne la sua vista, e cotal si moria;
[32m[K28[m[K[36m[K:[m[Kintorno ad esso era il grande Assüero,
[32m[K29[m[K[36m[K:[m[KEstèr sua sposa e ’l giusto Mardoceo,
[32m[K30[m[K[36m[K:[m[Kche fu al dire e al far così intero.
[32m[K31[m[K[36m[K:[m[KE come questa imagine rompeo
[32m[K32[m[K[36m[K:[m[Ksé per[01;31m[K sé [m[Kstessa, a guisa d’una bulla
[32m[K33[m[K[36m[K:[m[Kcui manca l’acqua sotto qual si feo,
[32m[K34[m[K[36m[K:[m[Ksurse in[01;31m[K mia[m[K visïone una fanciulla
[32m[K35[m[K[36m[K:[m[Kpiangendo forte, e dicea: «O regina,
[32m[K36[m[K[36m[K:[m[Kperché per ira hai voluto esser nulla?
[32m[K37[m[K[36m[K:[m[KAncisa t’hai per[01;31m[K non [m[Kperder Lavina;
[32m[K38[m[K[36m[K:[m[Kor m’hai perduta! Io son essa che lutto,
[32m[K39[m[K[36m[K:[m[Kmadre, a la tua pria ch’a l’altrui ruina».
[32m[K40[m[K[36m[K:[m[KCome si frange il sonno ove di butto
[32m[K41[m[K[36m[K:[m[Knova luce percuote il viso chiuso,
[32m[K42[m[K[36m[K:[m[Kche fratto guizza pria che muoia tutto;
[32m[K43[m[K[36m[K:[m[Kcosì l’imaginar mio cadde giuso
[32m[K44[m[K[36m[K:[m[Ktosto che lume il volto mi percosse,
[32m[K45[m[K[36m[K:[m[Kmaggior assai che quel ch’è in nostro uso.
[32m[K46[m[K[36m[K:[m[KI’ mi volgea per veder ov’ io fosse,
[32m[K47[m[K[36m[K:[m[Kquando una voce disse «Qui si monta»,
[32m[K48[m[K[36m[K:[m[Kche da ogne altro intento mi rimosse;
[32m[K49[m[K[36m[K:[m[Ke fece la[01;31m[K mia[m[K voglia tanto pronta
[32m[K50[m[K[36m[K:[m[Kdi riguardar chi era che parlava,
[32m[K51[m[K[36m[K:[m[Kche mai[01;31m[K non [m[Kposa, se[01;31m[K non [m[Ksi raffronta.
[32m[K52[m[K[36m[K:[m[KMa come al sol che nostra vista grava
[32m[K53[m[K[36m[K:[m[Ke per soverchio sua figura vela,
[32m[K54[m[K[36m[K:[m[Kcosì la[01;31m[K mia[m[K virtù quivi mancava.
[32m[K55[m[K[36m[K:[m[K«Questo è divino spirito, che[01;31m[K ne [m[Kla
[32m[K56[m[K[36m[K:[m[Kvia da ir sù[01;31m[K ne [m[Kdrizza sanza prego,
[32m[K57[m[K[36m[K:[m[Ke col suo lume[01;31m[K sé [m[Kmedesmo cela.
[32m[K58[m[K[36m[K:[m[KSì fa con noi, come l’uom si fa sego;
[32m[K59[m[K[36m[K:[m[Kché quale aspetta prego e l’uopo vede,
[32m[K60[m[K[36m[K:[m[Kmalignamente già si mette al nego.
[32m[K61[m[K[36m[K:[m[KOr accordiamo a tanto invito il piede;
[32m[K62[m[K[36m[K:[m[Kprocacciam di salir pria che s’abbui,
[32m[K63[m[K[36m[K:[m[Kché poi[01;31m[K non [m[Ksi poria, se ’l dì[01;31m[K non [m[Kriede».
[32m[K64[m[K[36m[K:[m[KCosì disse il mio duca, e io con lui
[32m[K65[m[K[36m[K:[m[Kvolgemmo i nostri passi ad una scala;
[32m[K66[m[K[36m[K:[m[Ke tosto ch’io al primo grado fui,
[32m[K67[m[K[36m[K:[m[Ksenti’mi presso quasi un muover d’ala
[32m[K68[m[K[36m[K:[m[Ke ventarmi nel viso e dir: ‘Beati
[32m[K69[m[K[36m[K:[m[Kpacifici, che son sanz’ ira mala!’.
[32m[K70[m[K[36m[K:[m[KGià eran sovra noi tanto levati
[32m[K71[m[K[36m[K:[m[Kli ultimi raggi che la notte segue,
[32m[K72[m[K[36m[K:[m[Kche le stelle apparivan da più lati.
[32m[K73[m[K[36m[K:[m[K‘O virtù[01;31m[K mia[m[K, perché sì ti dilegue?’,
[32m[K74[m[K[36m[K:[m[Kfra me stesso dicea, ché mi sentiva
[32m[K75[m[K[36m[K:[m[Kla possa de le gambe posta in triegue.
[32m[K76[m[K[36m[K:[m[KNoi eravam dove più[01;31m[K non [m[Ksaliva
[32m[K77[m[K[36m[K:[m[Kla scala sù, ed eravamo affissi,
[32m[K78[m[K[36m[K:[m[Kpur come nave ch’a la piaggia arriva.
[32m[K79[m[K[36m[K:[m[KE io attesi un poco, s’io udissi
[32m[K80[m[K[36m[K:[m[Kalcuna cosa nel novo girone;
[32m[K81[m[K[36m[K:[m[Kpoi mi volsi al maestro mio, e dissi:
[32m[K82[m[K[36m[K:[m[K«Dolce mio padre, dì, quale offensione
[32m[K83[m[K[36m[K:[m[Ksi purga qui nel giro dove semo?
[32m[K84[m[K[36m[K:[m[KSe i piè si stanno, non stea tuo sermone».
[32m[K85[m[K[36m[K:[m[KEd elli a me: «L’amor del bene, scemo
[32m[K86[m[K[36m[K:[m[Kdel suo dover, quiritta si ristora;
[32m[K87[m[K[36m[K:[m[Kqui si ribatte il mal tardato remo.
[32m[K88[m[K[36m[K:[m[KMa perché più aperto intendi ancora,
[32m[K89[m[K[36m[K:[m[Kvolgi la mente a me, e prenderai
[32m[K90[m[K[36m[K:[m[Kalcun buon frutto di nostra dimora».
[32m[K91[m[K[36m[K:[m[K«Né creator né creatura mai»,
[32m[K92[m[K[36m[K:[m[Kcominciò el, «figliuol, fu sanza amore,
[32m[K93[m[K[36m[K:[m[Ko naturale[01;31m[K o [m[Kd’animo; e tu ’l sai.
[32m[K94[m[K[36m[K:[m[KLo naturale è sempre sanza errore,
[32m[K95[m[K[36m[K:[m[Kma l’altro puote errar per malo obietto
[32m[K96[m[K[36m[K:[m[Ko per troppo[01;31m[K o [m[Kper poco di vigore.
[32m[K97[m[K[36m[K:[m[KMentre ch’elli è nel primo ben diretto,
[32m[K98[m[K[36m[K:[m[Ke ne’ secondi[01;31m[K sé [m[Kstesso misura,
[32m[K99[m[K[36m[K:[m[Kesser[01;31m[K non [m[Kpuò cagion di mal diletto;
[32m[K100[m[K[36m[K:[m[Kma quando al mal si torce, o con più cura
[32m[K101[m[K[36m[K:[m[Ko con men che[01;31m[K non [m[Kdee corre nel bene,
[32m[K102[m[K[36m[K:[m[Kcontra ’l fattore adovra sua fattura.
[32m[K103[m[K[36m[K:[m[KQuinci comprender puoi ch’esser convene
[32m[K104[m[K[36m[K:[m[Kamor sementa in voi d’ogne virtute
[32m[K105[m[K[36m[K:[m[Ke d’ogne operazion che merta pene.
[32m[K106[m[K[36m[K:[m[KOr, perché mai[01;31m[K non [m[Kpuò da la salute
[32m[K107[m[K[36m[K:[m[Kamor del suo subietto volger viso,
[32m[K108[m[K[36m[K:[m[Kda l’odio proprio son le cose tute;
[32m[K109[m[K[36m[K:[m[Ke perché intender[01;31m[K non [m[Ksi può diviso,
[32m[K110[m[K[36m[K:[m[Ke per[01;31m[K sé [m[Kstante, alcuno esser dal primo,
[32m[K111[m[K[36m[K:[m[Kda quello odiare ogne effetto è deciso.
[32m[K112[m[K[36m[K:[m[KResta, se dividendo bene stimo,
[32m[K113[m[K[36m[K:[m[Kche ’l mal che s’ama è del prossimo; ed esso
[32m[K114[m[K[36m[K:[m[Kamor nasce in tre modi in vostro limo.
[32m[K115[m[K[36m[K:[m[KÈ chi, per esser suo vicin soppresso,
[32m[K116[m[K[36m[K:[m[Kspera eccellenza, e sol per questo brama
[32m[K117[m[K[36m[K:[m[Kch’el sia di sua grandezza in basso messo;
[32m[K118[m[K[36m[K:[m[Kè chi podere, grazia, onore e fama
[32m[K119[m[K[36m[K:[m[Kteme di perder perch’ altri sormonti,
[32m[K120[m[K[36m[K:[m[Konde s’attrista sì che ’l contrario ama;
[32m[K121[m[K[36m[K:[m[Ked è chi per ingiuria par ch’aonti,
[32m[K122[m[K[36m[K:[m[Ksì che si fa de la vendetta ghiotto,
[32m[K123[m[K[36m[K:[m[Ke tal convien che ’l male altrui impronti.
[32m[K124[m[K[36m[K:[m[KQuesto triforme amor qua giù di sotto
[32m[K125[m[K[36m[K:[m[Ksi piange: or vo’ che tu de l’altro intende,
[32m[K126[m[K[36m[K:[m[Kche corre al ben con ordine corrotto.
[32m[K127[m[K[36m[K:[m[KCiascun confusamente un bene apprende
[32m[K128[m[K[36m[K:[m[Knel qual si queti l’animo, e disira;
[32m[K129[m[K[36m[K:[m[Kper che di giugner lui ciascun contende.
[32m[K130[m[K[36m[K:[m[KSe lento amore a lui veder vi tira
[32m[K131[m[K[36m[K:[m[Ko a lui acquistar, questa cornice,
[32m[K132[m[K[36m[K:[m[Kdopo giusto penter, ve[01;31m[K ne [m[Kmartira.
[32m[K133[m[K[36m[K:[m[KAltro ben è che[01;31m[K non [m[Kfa l’uom felice;
[32m[K134[m[K[36m[K:[m[Knon è felicità, non è la buona
[32m[K135[m[K[36m[K:[m[Kessenza, d’ogne ben frutto e radice.
[32m[K136[m[K[36m[K:[m[KL’amor ch’ad esso troppo s’abbandona,
[32m[K137[m[K[36m[K:[m[Kdi sovr’ a noi si piange per tre cerchi;
[32m[K138[m[K[36m[K:[m[Kma come tripartito si ragiona,
[32m[K139[m[K[36m[K:[m[Ktacciolo, acciò che tu per te[01;31m[K ne [m[Kcerchi».
