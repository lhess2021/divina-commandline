[32m[K1[m[K[36m[K:[m[KGodi, Fiorenza, poi che se’ sì grande
[32m[K2[m[K[36m[K:[m[Kche per mare e per terra batti l’ali,
[32m[K3[m[K[36m[K:[m[Ke per lo ’nferno tuo nome si spande!
[32m[K4[m[K[36m[K:[m[KTra li ladron trovai cinque cotali
[32m[K5[m[K[36m[K:[m[Ktuoi cittadini onde mi ven vergogna,
[32m[K6[m[K[36m[K:[m[Ke tu in grande orranza[01;31m[K non [m[Kne sali.
[32m[K7[m[K[36m[K:[m[KMa se presso al mattin del ver si sogna,
[32m[K8[m[K[36m[K:[m[Ktu sentirai, di qua da picciol tempo,
[32m[K9[m[K[36m[K:[m[Kdi quel che Prato, non ch’altri, t’agogna.
[32m[K10[m[K[36m[K:[m[KE se già fosse, non saria per tempo.
[32m[K11[m[K[36m[K:[m[KCosì foss’ ei, da che pur esser dee!
[32m[K12[m[K[36m[K:[m[Kché[01;31m[K più [m[Kmi graverà, com’ più m’attempo.
[32m[K13[m[K[36m[K:[m[KNoi ci partimmo, e su per le scalee
[32m[K14[m[K[36m[K:[m[Kche n’avean fatto iborni a scender pria,
[32m[K15[m[K[36m[K:[m[Krimontò ’l duca mio e trasse mee;
[32m[K16[m[K[36m[K:[m[Ke proseguendo la solinga via,
[32m[K17[m[K[36m[K:[m[Ktra le schegge e tra ’ rocchi de lo scoglio
[32m[K18[m[K[36m[K:[m[Klo piè sanza la man[01;31m[K non [m[Ksi spedia.
[32m[K19[m[K[36m[K:[m[KAllor mi dolsi, e ora mi ridoglio
[32m[K20[m[K[36m[K:[m[Kquando drizzo la mente a ciò ch’io vidi,
[32m[K21[m[K[36m[K:[m[Ke[01;31m[K più [m[Klo ’ngegno affreno ch’i’ non soglio,
[32m[K22[m[K[36m[K:[m[Kperché[01;31m[K non [m[Kcorra che virtù nol guidi;
[32m[K23[m[K[36m[K:[m[Ksì che, se stella bona o miglior cosa
[32m[K24[m[K[36m[K:[m[Km’ha dato ’l ben, ch’io stessi nol m’invidi.
[32m[K25[m[K[36m[K:[m[KQuante ’l villan ch’al poggio si riposa,
[32m[K26[m[K[36m[K:[m[Knel tempo che colui che ’l mondo schiara
[32m[K27[m[K[36m[K:[m[Kla faccia sua a noi tien meno ascosa,
[32m[K28[m[K[36m[K:[m[Kcome la mosca cede a la zanzara,
[32m[K29[m[K[36m[K:[m[Kvede lucciole giù per la vallea,
[32m[K30[m[K[36m[K:[m[Kforse colà dov’ e’ vendemmia e ara:
[32m[K31[m[K[36m[K:[m[Kdi tante fiamme tutta risplendea
[32m[K32[m[K[36m[K:[m[Kl’ottava bolgia, sì com’ io m’accorsi
[32m[K33[m[K[36m[K:[m[Ktosto che fui là ’ve ’l fondo parea.
[32m[K34[m[K[36m[K:[m[KE qual colui che si vengiò con li orsi
[32m[K35[m[K[36m[K:[m[Kvide ’l carro d’Elia al dipartire,
[32m[K36[m[K[36m[K:[m[Kquando i cavalli al cielo erti levorsi,
[32m[K37[m[K[36m[K:[m[Kche nol potea sì con li occhi seguire,
[32m[K38[m[K[36m[K:[m[Kch’el vedesse altro che la[01;31m[K fiamma[m[K sola,
[32m[K39[m[K[36m[K:[m[Ksì come nuvoletta, in sù salire:
[32m[K40[m[K[36m[K:[m[Ktal si move ciascuna per la gola
[32m[K41[m[K[36m[K:[m[Kdel fosso, ché nessuna mostra ’l furto,
[32m[K42[m[K[36m[K:[m[Ke ogne[01;31m[K fiamma[m[K un peccatore invola.
[32m[K43[m[K[36m[K:[m[KIo stava sovra ’l ponte a veder surto,
[32m[K44[m[K[36m[K:[m[Ksì che s’io[01;31m[K non [m[Kavessi un ronchion preso,
[32m[K45[m[K[36m[K:[m[Kcaduto sarei giù sanz’ esser urto.
[32m[K46[m[K[36m[K:[m[KE ’l duca che mi vide tanto atteso,
[32m[K47[m[K[36m[K:[m[Kdisse: «Dentro dai fuochi son li spirti;
[32m[K48[m[K[36m[K:[m[Kcatun si fascia di quel ch’elli è inceso».
[32m[K49[m[K[36m[K:[m[K«Maestro mio», rispuos’ io, «per udirti
[32m[K50[m[K[36m[K:[m[Kson[01;31m[K io [m[Kpiù certo; ma già m’era avviso
[32m[K51[m[K[36m[K:[m[Kche così fosse, e già voleva dirti:
[32m[K52[m[K[36m[K:[m[Kchi è ’n quel foco che vien sì diviso
[32m[K53[m[K[36m[K:[m[Kdi sopra, che par surger de la pira
[32m[K54[m[K[36m[K:[m[Kdov’ Eteòcle col fratel fu miso?».
[32m[K55[m[K[36m[K:[m[KRispuose a me: «Là dentro si martira
[32m[K56[m[K[36m[K:[m[KUlisse e Dïomede, e così insieme
[32m[K57[m[K[36m[K:[m[Ka la vendetta vanno come a l’ira;
[32m[K58[m[K[36m[K:[m[Ke dentro da la lor[01;31m[K fiamma[m[K si geme
[32m[K59[m[K[36m[K:[m[Kl’agguato del caval che fé la porta
[32m[K60[m[K[36m[K:[m[Konde uscì de’ Romani il gentil seme.
[32m[K61[m[K[36m[K:[m[KPiangevisi entro l’arte per che, morta,
[32m[K62[m[K[36m[K:[m[KDeïdamìa ancor si duol d’Achille,
[32m[K63[m[K[36m[K:[m[Ke del Palladio pena vi si porta».
[32m[K64[m[K[36m[K:[m[K«S’ei posson dentro da quelle faville
[32m[K65[m[K[36m[K:[m[Kparlar», diss’ io, «maestro, assai ten priego
[32m[K66[m[K[36m[K:[m[Ke ripriego, che ’l priego vaglia mille,
[32m[K67[m[K[36m[K:[m[Kche[01;31m[K non [m[Kmi facci de l’attender niego
[32m[K68[m[K[36m[K:[m[Kfin che la[01;31m[K fiamma[m[K cornuta qua vegna;
[32m[K69[m[K[36m[K:[m[Kvedi che del disio ver’ lei mi piego!».
[32m[K70[m[K[36m[K:[m[KEd elli a me: «La tua preghiera è degna
[32m[K71[m[K[36m[K:[m[Kdi molta loda, e[01;31m[K io [m[Kperò l’accetto;
[32m[K72[m[K[36m[K:[m[Kma fa che la tua lingua si sostegna.
[32m[K73[m[K[36m[K:[m[KLascia parlare a me, ch’i’ ho concetto
[32m[K74[m[K[36m[K:[m[Kciò che tu vuoi; ch’ei sarebbero schivi,
[32m[K75[m[K[36m[K:[m[Kperch’ e’ fuor greci, forse del tuo detto».
[32m[K76[m[K[36m[K:[m[KPoi che la[01;31m[K fiamma[m[K fu venuta quivi
[32m[K77[m[K[36m[K:[m[Kdove parve al mio duca tempo e loco,
[32m[K78[m[K[36m[K:[m[Kin questa forma lui parlare audivi:
[32m[K79[m[K[36m[K:[m[K«O voi che siete due dentro ad un foco,
[32m[K80[m[K[36m[K:[m[Ks’io meritai di voi mentre ch’io vissi,
[32m[K81[m[K[36m[K:[m[Ks’io meritai di voi assai o poco
[32m[K82[m[K[36m[K:[m[Kquando nel mondo li alti versi scrissi,
[32m[K83[m[K[36m[K:[m[Knon vi movete; ma l’un di voi dica
[32m[K84[m[K[36m[K:[m[Kdove, per lui, perduto a morir gissi».
[32m[K85[m[K[36m[K:[m[KLo maggior corno de la[01;31m[K fiamma[m[K antica
[32m[K86[m[K[36m[K:[m[Kcominciò a crollarsi mormorando,
[32m[K87[m[K[36m[K:[m[Kpur come quella cui vento affatica;
[32m[K88[m[K[36m[K:[m[Kindi la cima qua e là menando,
[32m[K89[m[K[36m[K:[m[Kcome fosse la lingua che parlasse,
[32m[K90[m[K[36m[K:[m[Kgittò voce di fuori e disse: «Quando
[32m[K91[m[K[36m[K:[m[Kmi diparti’ da Circe, che sottrasse
[32m[K92[m[K[36m[K:[m[Kme[01;31m[K più [m[Kd’un anno là presso a Gaeta,
[32m[K93[m[K[36m[K:[m[Kprima che sì Enëa la nomasse,
[32m[K94[m[K[36m[K:[m[Kné dolcezza di figlio, né la pieta
[32m[K95[m[K[36m[K:[m[Kdel vecchio padre, né ’l debito amore
[32m[K96[m[K[36m[K:[m[Klo qual dovea Penelopè far lieta,
[32m[K97[m[K[36m[K:[m[Kvincer potero dentro a[01;31m[K me [m[Kl’ardore
[32m[K98[m[K[36m[K:[m[Kch’i’ ebbi a divenir del mondo esperto
[32m[K99[m[K[36m[K:[m[Ke de li vizi umani e del valore;
[32m[K100[m[K[36m[K:[m[Kma misi[01;31m[K me [m[Kper l’alto mare aperto
[32m[K101[m[K[36m[K:[m[Ksol con un legno e con quella compagna
[32m[K102[m[K[36m[K:[m[Kpicciola da la qual[01;31m[K non [m[Kfui diserto.
[32m[K103[m[K[36m[K:[m[KL’un lito e l’altro vidi infin la Spagna,
[32m[K104[m[K[36m[K:[m[Kfin nel Morrocco, e l’isola d’i Sardi,
[32m[K105[m[K[36m[K:[m[Ke l’altre che quel mare intorno bagna.
[32m[K106[m[K[36m[K:[m[KIo e ’ compagni eravam vecchi e tardi
[32m[K107[m[K[36m[K:[m[Kquando venimmo a quella foce stretta
[32m[K108[m[K[36m[K:[m[Kdov’ Ercule segnò li suoi riguardi
[32m[K109[m[K[36m[K:[m[Kacciò che l’uom[01;31m[K più [m[Koltre[01;31m[K non [m[Ksi metta;
[32m[K110[m[K[36m[K:[m[Kda la man destra mi lasciai Sibilia,
[32m[K111[m[K[36m[K:[m[Kda l’altra già m’avea lasciata Setta.
[32m[K112[m[K[36m[K:[m[K“O frati”, dissi, “che per cento milia
[32m[K113[m[K[36m[K:[m[Kperigli siete giunti a l’occidente,
[32m[K114[m[K[36m[K:[m[Ka questa tanto picciola vigilia
[32m[K115[m[K[36m[K:[m[Kd’i nostri sensi ch’è del rimanente
[32m[K116[m[K[36m[K:[m[Knon vogliate negar l’esperïenza,
[32m[K117[m[K[36m[K:[m[Kdi retro al sol, del mondo sanza gente.
[32m[K118[m[K[36m[K:[m[KConsiderate la vostra semenza:
[32m[K119[m[K[36m[K:[m[Kfatti[01;31m[K non [m[Kfoste a viver come bruti,
[32m[K120[m[K[36m[K:[m[Kma per seguir virtute e canoscenza”.
[32m[K121[m[K[36m[K:[m[KLi miei compagni fec’ io sì aguti,
[32m[K122[m[K[36m[K:[m[Kcon questa orazion picciola, al cammino,
[32m[K123[m[K[36m[K:[m[Kche a pena poscia li avrei ritenuti;
[32m[K124[m[K[36m[K:[m[Ke volta nostra poppa nel mattino,
[32m[K125[m[K[36m[K:[m[Kde’ remi facemmo ali al folle volo,
[32m[K126[m[K[36m[K:[m[Ksempre acquistando dal lato mancino.
[32m[K127[m[K[36m[K:[m[KTutte le stelle già de l’altro polo
[32m[K128[m[K[36m[K:[m[Kvedea la notte, e ’l nostro tanto basso,
[32m[K129[m[K[36m[K:[m[Kche[01;31m[K non [m[Ksurgëa fuor del marin suolo.
[32m[K130[m[K[36m[K:[m[KCinque volte racceso e tante casso
[32m[K131[m[K[36m[K:[m[Klo lume era di sotto da la luna,
[32m[K132[m[K[36m[K:[m[Kpoi che ’ntrati eravam ne l’alto passo,
[32m[K133[m[K[36m[K:[m[Kquando n’apparve una montagna, bruna
[32m[K134[m[K[36m[K:[m[Kper la distanza, e parvemi alta tanto
[32m[K135[m[K[36m[K:[m[Kquanto veduta[01;31m[K non [m[Kavëa alcuna.
[32m[K136[m[K[36m[K:[m[KNoi ci allegrammo, e tosto tornò in pianto;
[32m[K137[m[K[36m[K:[m[Kché de la nova terra un turbo nacque
[32m[K138[m[K[36m[K:[m[Ke percosse del legno il primo canto.
[32m[K139[m[K[36m[K:[m[KTre volte il fé girar con tutte l’acque;
[32m[K140[m[K[36m[K:[m[Ka la quarta levar la poppa in suso
[32m[K141[m[K[36m[K:[m[Ke la prora ire in giù, com’ altrui piacque,
[32m[K142[m[K[36m[K:[m[Kinfin che ’l mar fu sovra noi richiuso».
