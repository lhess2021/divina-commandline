[32m[K1[m[K[36m[K:[m[KPoscia che ’ncontro a la vita presente
[32m[K2[m[K[36m[K:[m[Kd’i miseri mortali aperse ’l vero
[32m[K3[m[K[36m[K:[m[Kquella che ’mparadisa la[01;31m[K mia [m[Kmente,
[32m[K4[m[K[36m[K:[m[Kcome in lo specchio fiamma di doppiero
[32m[K5[m[K[36m[K:[m[Kvede colui che[01;31m[K se [m[Kn’alluma retro,
[32m[K6[m[K[36m[K:[m[Kprima che l’abbia in vista o in pensiero,
[32m[K7[m[K[36m[K:[m[Ke sé rivolge per veder se ’l vetro
[32m[K8[m[K[36m[K:[m[Kli dice il vero, e vede ch’el s’accorda
[32m[K9[m[K[36m[K:[m[Kcon esso come nota con[01;31m[K suo [m[Kmetro;
[32m[K10[m[K[36m[K:[m[Kcosì la[01;31m[K mia [m[Kmemoria si ricorda
[32m[K11[m[K[36m[K:[m[Kch’io feci riguardando ne’ belli occhi
[32m[K12[m[K[36m[K:[m[Konde a pigliarmi fece Amor la corda.
[32m[K13[m[K[36m[K:[m[KE com’ io mi rivolsi e furon tocchi
[32m[K14[m[K[36m[K:[m[Kli miei da ciò che pare in quel volume,
[32m[K15[m[K[36m[K:[m[Kquandunque nel[01;31m[K suo [m[Kgiro ben s’adocchi,
[32m[K16[m[K[36m[K:[m[Kun punto vidi che raggiava lume
[32m[K17[m[K[36m[K:[m[Kacuto sì, che ’l viso ch’elli affoca
[32m[K18[m[K[36m[K:[m[Kchiuder conviensi per lo forte acume;
[32m[K19[m[K[36m[K:[m[Ke quale stella par quinci più poca,
[32m[K20[m[K[36m[K:[m[Kparrebbe luna, locata con esso
[32m[K21[m[K[36m[K:[m[Kcome stella con stella si collòca.
[32m[K22[m[K[36m[K:[m[KForse cotanto[01;31m[K quanto[m[K pare appresso
[32m[K23[m[K[36m[K:[m[Kalo cigner la luce che ’l dipigne
[32m[K24[m[K[36m[K:[m[Kquando ’l vapor che ’l porta più è spesso,
[32m[K25[m[K[36m[K:[m[Kdistante intorno al punto un cerchio d’igne
[32m[K26[m[K[36m[K:[m[Ksi girava sì ratto, ch’avria vinto
[32m[K27[m[K[36m[K:[m[Kquel moto che più tosto il mondo cigne;
[32m[K28[m[K[36m[K:[m[Ke questo era d’un altro circumcinto,
[32m[K29[m[K[36m[K:[m[Ke quel dal terzo, e ’l terzo poi dal quarto,
[32m[K30[m[K[36m[K:[m[Kdal quinto il quarto, e poi dal sesto il quinto.
[32m[K31[m[K[36m[K:[m[KSopra seguiva il settimo sì sparto
[32m[K32[m[K[36m[K:[m[Kgià di larghezza, che ’l messo di Iuno
[32m[K33[m[K[36m[K:[m[Kintero a contenerlo sarebbe arto.
[32m[K34[m[K[36m[K:[m[KCosì l’ottavo e ’l nono; e chiascheduno
[32m[K35[m[K[36m[K:[m[Kpiù tardo si movea, secondo ch’era
[32m[K36[m[K[36m[K:[m[Kin numero distante più da l’uno;
[32m[K37[m[K[36m[K:[m[Ke quello avea la fiamma più sincera
[32m[K38[m[K[36m[K:[m[Kcui men distava la favilla pura,
[32m[K39[m[K[36m[K:[m[Kcredo, però che più di lei s’invera.
[32m[K40[m[K[36m[K:[m[KLa donna mia, che mi vedëa in cura
[32m[K41[m[K[36m[K:[m[Kforte sospeso, disse: «Da quel punto
[32m[K42[m[K[36m[K:[m[Kdepende il cielo e tutta la natura.
[32m[K43[m[K[36m[K:[m[KMira quel cerchio che più li è congiunto;
[32m[K44[m[K[36m[K:[m[Ke sappi che ’l[01;31m[K suo [m[Kmuovere è sì tosto
[32m[K45[m[K[36m[K:[m[Kper l’affocato amore ond’ elli è punto».
[32m[K46[m[K[36m[K:[m[KE io a lei: «Se ’l mondo fosse posto
[32m[K47[m[K[36m[K:[m[Kcon l’ordine ch’io veggio in quelle rote,
[32m[K48[m[K[36m[K:[m[Ksazio m’avrebbe ciò che m’è proposto;
[32m[K49[m[K[36m[K:[m[Kma nel mondo sensibile si puote
[32m[K50[m[K[36m[K:[m[Kveder le volte tanto più divine,
[32m[K51[m[K[36m[K:[m[Kquant’ elle son dal centro più remote.
[32m[K52[m[K[36m[K:[m[KOnde, se ’l mio disir dee aver fine
[32m[K53[m[K[36m[K:[m[Kin questo miro e angelico templo
[32m[K54[m[K[36m[K:[m[Kche solo amore e luce ha per confine,
[32m[K55[m[K[36m[K:[m[Kudir convienmi ancor come l’essemplo
[32m[K56[m[K[36m[K:[m[Ke l’essemplare[01;31m[K non [m[Kvanno d’un modo,
[32m[K57[m[K[36m[K:[m[Kché io per me indarno a ciò contemplo».
[32m[K58[m[K[36m[K:[m[K«Se li tuoi diti[01;31m[K non [m[Ksono a tal nodo
[32m[K59[m[K[36m[K:[m[Ksufficïenti, non è maraviglia:
[32m[K60[m[K[36m[K:[m[Ktanto, per[01;31m[K non [m[Ktentare, è fatto sodo!».
[32m[K61[m[K[36m[K:[m[KCosì la donna mia; poi disse: «Piglia
[32m[K62[m[K[36m[K:[m[Kquel ch’io ti dicerò, se vuo’ saziarti;
[32m[K63[m[K[36m[K:[m[Ke intorno da esso t’assottiglia.
[32m[K64[m[K[36m[K:[m[KLi cerchi corporai sono ampi e arti
[32m[K65[m[K[36m[K:[m[Ksecondo il più e ’l men de la virtute
[32m[K66[m[K[36m[K:[m[Kche si distende per tutte lor parti.
[32m[K67[m[K[36m[K:[m[KMaggior bontà vuol far maggior salute;
[32m[K68[m[K[36m[K:[m[Kmaggior salute maggior corpo cape,
[32m[K69[m[K[36m[K:[m[Ks’elli ha le parti igualmente compiute.
[32m[K70[m[K[36m[K:[m[KDunque costui che tutto[01;31m[K quanto[m[K rape
[32m[K71[m[K[36m[K:[m[Kl’altro universo seco, corrisponde
[32m[K72[m[K[36m[K:[m[Kal cerchio che più ama e che più sape:
[32m[K73[m[K[36m[K:[m[Kper che, se tu a la virtù circonde
[32m[K74[m[K[36m[K:[m[Kla tua misura, non a la parvenza
[32m[K75[m[K[36m[K:[m[Kde le sustanze che t’appaion tonde,
[32m[K76[m[K[36m[K:[m[Ktu vederai mirabil consequenza
[32m[K77[m[K[36m[K:[m[Kdi maggio a più e di minore a meno,
[32m[K78[m[K[36m[K:[m[Kin ciascun cielo, a süa intelligenza».
[32m[K79[m[K[36m[K:[m[KCome rimane splendido e sereno
[32m[K80[m[K[36m[K:[m[Kl’emisperio de l’aere, quando soffia
[32m[K81[m[K[36m[K:[m[KBorea da quella guancia ond’ è più leno,
[32m[K82[m[K[36m[K:[m[Kper che si purga e risolve la roffia
[32m[K83[m[K[36m[K:[m[Kche pria turbava, sì che ’l ciel ne ride
[32m[K84[m[K[36m[K:[m[Kcon le bellezze d’ogne sua paroffia;
[32m[K85[m[K[36m[K:[m[Kcosì fec’ïo, poi che mi provide
[32m[K86[m[K[36m[K:[m[Kla donna[01;31m[K mia [m[Kdel[01;31m[K suo [m[Krisponder chiaro,
[32m[K87[m[K[36m[K:[m[Ke come stella in cielo il ver si vide.
[32m[K88[m[K[36m[K:[m[KE poi che le parole sue restaro,
[32m[K89[m[K[36m[K:[m[Knon altrimenti ferro disfavilla
[32m[K90[m[K[36m[K:[m[Kche bolle, come i cerchi sfavillaro.
[32m[K91[m[K[36m[K:[m[KL’incendio[01;31m[K suo [m[Kseguiva ogne scintilla;
[32m[K92[m[K[36m[K:[m[Ked eran tante, che ’l numero loro
[32m[K93[m[K[36m[K:[m[Kpiù che ’l doppiar de li scacchi s’inmilla.
[32m[K94[m[K[36m[K:[m[KIo sentiva osannar di coro in coro
[32m[K95[m[K[36m[K:[m[Kal punto fisso che li tiene a li ubi,
[32m[K96[m[K[36m[K:[m[Ke terrà sempre, ne’ quai sempre fuoro.
[32m[K97[m[K[36m[K:[m[KE quella che vedëa i pensier dubi
[32m[K98[m[K[36m[K:[m[Kne la[01;31m[K mia [m[Kmente, disse: «I cerchi primi
[32m[K99[m[K[36m[K:[m[Kt’hanno mostrato Serafi e Cherubi.
[32m[K100[m[K[36m[K:[m[KCosì veloci seguono i suoi vimi,
[32m[K101[m[K[36m[K:[m[Kper somigliarsi al punto[01;31m[K quanto[m[K ponno;
[32m[K102[m[K[36m[K:[m[Ke posson[01;31m[K quanto[m[K a veder son soblimi.
[32m[K103[m[K[36m[K:[m[KQuelli altri amori che ’ntorno li vonno,
[32m[K104[m[K[36m[K:[m[Ksi chiaman Troni del divino aspetto,
[32m[K105[m[K[36m[K:[m[Kper che ’l primo ternaro terminonno;
[32m[K106[m[K[36m[K:[m[Ke dei saper che tutti hanno diletto
[32m[K107[m[K[36m[K:[m[Kquanto la sua veduta si profonda
[32m[K108[m[K[36m[K:[m[Knel vero in che si queta ogne intelletto.
[32m[K109[m[K[36m[K:[m[KQuinci si può veder come si fonda
[32m[K110[m[K[36m[K:[m[Kl’esser beato ne l’atto che vede,
[32m[K111[m[K[36m[K:[m[Knon in quel ch’ama, che poscia seconda;
[32m[K112[m[K[36m[K:[m[Ke del vedere è misura mercede,
[32m[K113[m[K[36m[K:[m[Kche grazia partorisce e buona voglia:
[32m[K114[m[K[36m[K:[m[Kcosì di grado in grado si procede.
[32m[K115[m[K[36m[K:[m[KL’altro ternaro, che così germoglia
[32m[K116[m[K[36m[K:[m[Kin questa primavera sempiterna
[32m[K117[m[K[36m[K:[m[Kche notturno Arïete[01;31m[K non [m[Kdispoglia,
[32m[K118[m[K[36m[K:[m[Kperpetüalemente ‘Osanna’ sberna
[32m[K119[m[K[36m[K:[m[Kcon tre melode, che suonano in tree
[32m[K120[m[K[36m[K:[m[Kordini di letizia onde s’interna.
[32m[K121[m[K[36m[K:[m[KIn essa gerarcia son l’altre dee:
[32m[K122[m[K[36m[K:[m[Kprima Dominazioni, e poi Virtudi;
[32m[K123[m[K[36m[K:[m[Kl’ordine terzo di Podestadi èe.
[32m[K124[m[K[36m[K:[m[KPoscia ne’ due penultimi tripudi
[32m[K125[m[K[36m[K:[m[KPrincipati e Arcangeli si girano;
[32m[K126[m[K[36m[K:[m[Kl’ultimo è tutto d’Angelici ludi.
[32m[K127[m[K[36m[K:[m[KQuesti ordini di sù tutti s’ammirano,
[32m[K128[m[K[36m[K:[m[Ke di giù vincon sì, che verso Dio
[32m[K129[m[K[36m[K:[m[Ktutti tirati sono e tutti tirano.
[32m[K130[m[K[36m[K:[m[KE Dïonisio con tanto disio
[32m[K131[m[K[36m[K:[m[Ka contemplar questi ordini si mise,
[32m[K132[m[K[36m[K:[m[Kche li nomò e distinse com’ io.
[32m[K133[m[K[36m[K:[m[KMa Gregorio da lui poi si divise;
[32m[K134[m[K[36m[K:[m[Konde, sì tosto come li occhi aperse
[32m[K135[m[K[36m[K:[m[Kin questo ciel, di sé medesmo rise.
[32m[K136[m[K[36m[K:[m[KE[01;31m[K se [m[Ktanto secreto ver proferse
[32m[K137[m[K[36m[K:[m[Kmortale in terra, non voglio ch’ammiri:
[32m[K138[m[K[36m[K:[m[Kché chi ’l vide qua sù gliel discoperse
[32m[K139[m[K[36m[K:[m[Kcon altro assai del ver di questi giri».
