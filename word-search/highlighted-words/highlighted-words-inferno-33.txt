[32m[K1[m[K[36m[K:[m[K﻿La bocca sollevò dal fiero pasto
[32m[K2[m[K[36m[K:[m[Kquel peccator, forbendola a’ capelli
[32m[K3[m[K[36m[K:[m[Kdel capo ch’elli avea di retro guasto.
[32m[K4[m[K[36m[K:[m[KPoi cominciò: «Tu vuo’ ch’io rinovelli
[32m[K5[m[K[36m[K:[m[Kdisperato dolor che ’l cor mi preme
[32m[K6[m[K[36m[K:[m[Kgià pur pensando, pria ch’io ne favelli.
[32m[K7[m[K[36m[K:[m[KMa[01;31m[K se[m[K le mie parole esser dien seme
[32m[K8[m[K[36m[K:[m[Kche frutti infamia al traditor ch’i’ rodo,
[32m[K9[m[K[36m[K:[m[Kparlar e lagrimar vedrai insieme.
[32m[K10[m[K[36m[K:[m[KIo[01;31m[K non [m[Kso chi tu[01;31m[K se[m[K’ né per che modo
[32m[K11[m[K[36m[K:[m[Kvenuto[01;31m[K se[m[K’ qua giù; ma fiorentino
[32m[K12[m[K[36m[K:[m[Kmi sembri veramente quand’ io t’odo.
[32m[K13[m[K[36m[K:[m[KTu dei saper ch’i’ fui conte Ugolino,
[32m[K14[m[K[36m[K:[m[Ke questi è l’arcivescovo Ruggieri:
[32m[K15[m[K[36m[K:[m[Kor ti dirò perché i son tal vicino.
[32m[K16[m[K[36m[K:[m[KChe per l’effetto de’ suo’ mai pensieri,
[32m[K17[m[K[36m[K:[m[Kfidandomi di lui, io fossi preso
[32m[K18[m[K[36m[K:[m[Ke poscia morto, dir[01;31m[K non [m[Kè mestieri;
[32m[K19[m[K[36m[K:[m[Kperò quel che[01;31m[K non [m[Kpuoi avere inteso,
[32m[K20[m[K[36m[K:[m[Kcioè come la morte mia fu cruda,
[32m[K21[m[K[36m[K:[m[Kudirai, e saprai s’e’ m’ha offeso.
[32m[K22[m[K[36m[K:[m[KBreve pertugio dentro da la Muda,
[32m[K23[m[K[36m[K:[m[Kla qual per me ha ’l titol de la fame,
[32m[K24[m[K[36m[K:[m[Ke che conviene ancor ch’altrui si chiuda,
[32m[K25[m[K[36m[K:[m[Km’avea mostrato per lo[01;31m[K suo [m[Kforame
[32m[K26[m[K[36m[K:[m[Kpiù lune già, quand’ io feci ’l mal sonno
[32m[K27[m[K[36m[K:[m[Kche del futuro mi squarciò ’l velame.
[32m[K28[m[K[36m[K:[m[KQuesti pareva a me maestro e donno,
[32m[K29[m[K[36m[K:[m[Kcacciando il lupo e ’ lupicini al monte
[32m[K30[m[K[36m[K:[m[Kper che i Pisan veder Lucca[01;31m[K non [m[Kponno.
[32m[K31[m[K[36m[K:[m[KCon cagne magre, studïose e conte
[32m[K32[m[K[36m[K:[m[KGualandi con Sismondi e con Lanfranchi
[32m[K33[m[K[36m[K:[m[Ks’avea messi dinanzi da la fronte.
[32m[K34[m[K[36m[K:[m[KIn picciol corso mi parieno stanchi
[32m[K35[m[K[36m[K:[m[Klo padre e ’ figli, e con l’agute scane
[32m[K36[m[K[36m[K:[m[Kmi parea lor veder fender li fianchi.
[32m[K37[m[K[36m[K:[m[KQuando fui desto innanzi la dimane,
[32m[K38[m[K[36m[K:[m[Kpianger senti’ fra ’l sonno i miei figliuoli
[32m[K39[m[K[36m[K:[m[Kch’eran con meco, e dimandar del pane.
[32m[K40[m[K[36m[K:[m[KBen[01;31m[K se[m[K’ crudel, se tu già[01;31m[K non [m[Kti duoli
[32m[K41[m[K[36m[K:[m[Kpensando ciò che ’l[01;31m[K mio [m[Kcor s’annunziava;
[32m[K42[m[K[36m[K:[m[Ke[01;31m[K se[m[K[01;31m[K non [m[Kpiangi, di che pianger suoli?
[32m[K43[m[K[36m[K:[m[KGià eran desti, e l’ora s’appressava
[32m[K44[m[K[36m[K:[m[Kche ’l cibo ne solëa essere addotto,
[32m[K45[m[K[36m[K:[m[Ke per[01;31m[K suo [m[Ksogno ciascun dubitava;
[32m[K46[m[K[36m[K:[m[Ke[01;31m[K io [m[Ksenti’ chiavar l’uscio di sotto
[32m[K47[m[K[36m[K:[m[Ka l’orribile torre; ond’ io guardai
[32m[K48[m[K[36m[K:[m[Knel viso a’ mie’ figliuoi sanza far motto.
[32m[K49[m[K[36m[K:[m[KIo[01;31m[K non [m[Kpiangëa, sì dentro impetrai:
[32m[K50[m[K[36m[K:[m[Kpiangevan elli; e Anselmuccio mio
[32m[K51[m[K[36m[K:[m[Kdisse: “Tu guardi sì, padre! che hai?”.
[32m[K52[m[K[36m[K:[m[KPerciò[01;31m[K non [m[Klagrimai né rispuos’ io
[32m[K53[m[K[36m[K:[m[Ktutto quel giorno né la notte appresso,
[32m[K54[m[K[36m[K:[m[Kinfin che l’altro sol nel mondo uscìo.
[32m[K55[m[K[36m[K:[m[KCome un poco di raggio si fu messo
[32m[K56[m[K[36m[K:[m[Knel doloroso carcere, e[01;31m[K io [m[Kscorsi
[32m[K57[m[K[36m[K:[m[Kper quattro visi il[01;31m[K mio [m[Kaspetto stesso,
[32m[K58[m[K[36m[K:[m[Kambo le man per lo dolor mi morsi;
[32m[K59[m[K[36m[K:[m[Ked ei, pensando ch’io ’l fessi per voglia
[32m[K60[m[K[36m[K:[m[Kdi manicar, di sùbito levorsi
[32m[K61[m[K[36m[K:[m[Ke disser: “Padre, assai ci fia men doglia
[32m[K62[m[K[36m[K:[m[Kse tu mangi di noi: tu ne vestisti
[32m[K63[m[K[36m[K:[m[Kqueste misere carni, e tu le spoglia”.
[32m[K64[m[K[36m[K:[m[KQueta’mi allor per[01;31m[K non [m[Kfarli più tristi;
[32m[K65[m[K[36m[K:[m[Klo dì e l’altro stemmo tutti muti;
[32m[K66[m[K[36m[K:[m[Kahi dura terra, perché[01;31m[K non [m[Kt’apristi?
[32m[K67[m[K[36m[K:[m[KPoscia che fummo al quarto dì venuti,
[32m[K68[m[K[36m[K:[m[KGaddo mi si gittò disteso a’ piedi,
[32m[K69[m[K[36m[K:[m[Kdicendo: “Padre mio, ché[01;31m[K non [m[Km’aiuti?”.
[32m[K70[m[K[36m[K:[m[KQuivi morì; e come tu mi vedi,
[32m[K71[m[K[36m[K:[m[Kvid’ io cascar li tre ad uno ad uno
[32m[K72[m[K[36m[K:[m[Ktra ’l quinto dì e ’l sesto; ond’ io mi diedi,
[32m[K73[m[K[36m[K:[m[Kgià cieco, a brancolar sovra ciascuno,
[32m[K74[m[K[36m[K:[m[Ke due dì li chiamai, poi che fur morti.
[32m[K75[m[K[36m[K:[m[KPoscia, più che ’l dolor, poté ’l digiuno».
[32m[K76[m[K[36m[K:[m[KQuand’ ebbe detto ciò, con li occhi torti
[32m[K77[m[K[36m[K:[m[Kriprese ’l teschio misero co’ denti,
[32m[K78[m[K[36m[K:[m[Kche furo a l’osso, come d’un can, forti.
[32m[K79[m[K[36m[K:[m[KAhi Pisa, vituperio de le genti
[32m[K80[m[K[36m[K:[m[Kdel bel paese là dove ’l sì suona,
[32m[K81[m[K[36m[K:[m[Kpoi che i vicini a te punir son lenti,
[32m[K82[m[K[36m[K:[m[Kmuovasi la Capraia e la Gorgona,
[32m[K83[m[K[36m[K:[m[Ke faccian siepe ad Arno in su la foce,
[32m[K84[m[K[36m[K:[m[Ksì ch’elli annieghi in te ogne persona!
[32m[K85[m[K[36m[K:[m[KChe[01;31m[K se[m[K ’l conte Ugolino aveva voce
[32m[K86[m[K[36m[K:[m[Kd’aver tradita te de le castella,
[32m[K87[m[K[36m[K:[m[Knon dovei tu i figliuoi porre a tal croce.
[32m[K88[m[K[36m[K:[m[KInnocenti facea l’età novella,
[32m[K89[m[K[36m[K:[m[Knovella Tebe, Uguiccione e ’l Brigata
[32m[K90[m[K[36m[K:[m[Ke li altri due che ’l canto suso appella.
[32m[K91[m[K[36m[K:[m[KNoi passammo oltre, là ’ve la gelata
[32m[K92[m[K[36m[K:[m[Kruvidamente un’altra gente fascia,
[32m[K93[m[K[36m[K:[m[Knon volta in giù, ma tutta riversata.
[32m[K94[m[K[36m[K:[m[KLo pianto stesso lì pianger[01;31m[K non [m[Klascia,
[32m[K95[m[K[36m[K:[m[Ke ’l duol che truova in su li occhi rintoppo,
[32m[K96[m[K[36m[K:[m[Ksi volge in entro a far crescer l’ambascia;
[32m[K97[m[K[36m[K:[m[Kché le lagrime prime fanno groppo,
[32m[K98[m[K[36m[K:[m[Ke sì come visiere di cristallo,
[32m[K99[m[K[36m[K:[m[Krïempion sotto ’l ciglio tutto il coppo.
[32m[K100[m[K[36m[K:[m[KE avvegna che, sì come d’un callo,
[32m[K101[m[K[36m[K:[m[Kper la freddura ciascun sentimento
[32m[K102[m[K[36m[K:[m[Kcessato avesse del[01;31m[K mio [m[Kviso stallo,
[32m[K103[m[K[36m[K:[m[Kgià mi parea sentire alquanto vento;
[32m[K104[m[K[36m[K:[m[Kper ch’io: «Maestro mio, questo chi move?
[32m[K105[m[K[36m[K:[m[Knon è qua giù ogne vapore spento?».
[32m[K106[m[K[36m[K:[m[KOnd’ elli a me: «Avaccio sarai dove
[32m[K107[m[K[36m[K:[m[Kdi ciò ti farà l’occhio la risposta,
[32m[K108[m[K[36m[K:[m[Kveggendo la cagion che ’l fiato piove».
[32m[K109[m[K[36m[K:[m[KE un de’ tristi de la fredda crosta
[32m[K110[m[K[36m[K:[m[Kgridò a noi: «O anime crudeli
[32m[K111[m[K[36m[K:[m[Ktanto che data v’è l’ultima posta,
[32m[K112[m[K[36m[K:[m[Klevatemi dal viso i duri veli,
[32m[K113[m[K[36m[K:[m[Ksì ch’ïo sfoghi ’l duol che ’l cor m’impregna,
[32m[K114[m[K[36m[K:[m[Kun poco, pria che ’l pianto si raggeli».
[32m[K115[m[K[36m[K:[m[KPer ch’io a lui: «Se vuo’ ch’i’ ti sovvegna,
[32m[K116[m[K[36m[K:[m[Kdimmi chi[01;31m[K se[m[K’, e s’io[01;31m[K non [m[Kti disbrigo,
[32m[K117[m[K[36m[K:[m[Kal fondo de la ghiaccia ir mi convegna».
[32m[K118[m[K[36m[K:[m[KRispuose adunque: «I’ son frate Alberigo;
[32m[K119[m[K[36m[K:[m[Ki’ son quel da le frutta del mal orto,
[32m[K120[m[K[36m[K:[m[Kche qui riprendo dattero per figo».
[32m[K121[m[K[36m[K:[m[K«Oh», diss’ io lui, «or[01;31m[K se[m[K’ tu ancor morto?».
[32m[K122[m[K[36m[K:[m[KEd elli a me: «Come ’l[01;31m[K mio [m[Kcorpo stea
[32m[K123[m[K[36m[K:[m[Knel mondo sù, nulla scïenza porto.
[32m[K124[m[K[36m[K:[m[KCotal vantaggio ha questa Tolomea,
[32m[K125[m[K[36m[K:[m[Kche spesse volte l’anima ci cade
[32m[K126[m[K[36m[K:[m[Kinnanzi ch’Atropòs mossa le dea.
[32m[K127[m[K[36m[K:[m[KE perché tu più volentier mi rade
[32m[K128[m[K[36m[K:[m[Kle ’nvetrïate lagrime dal volto,
[32m[K129[m[K[36m[K:[m[Ksappie che, tosto che l’anima trade
[32m[K130[m[K[36m[K:[m[Kcome fec’ ïo, il corpo[01;31m[K suo [m[Kl’è tolto
[32m[K131[m[K[36m[K:[m[Kda un demonio, che poscia il governa
[32m[K132[m[K[36m[K:[m[Kmentre che ’l tempo[01;31m[K suo [m[Ktutto sia vòlto.
[32m[K133[m[K[36m[K:[m[KElla ruina in sì fatta cisterna;
[32m[K134[m[K[36m[K:[m[Ke forse pare ancor lo corpo suso
[32m[K135[m[K[36m[K:[m[Kde l’ombra che di qua dietro mi verna.
[32m[K136[m[K[36m[K:[m[KTu ’l dei saper, se tu vien pur mo giuso:
[32m[K137[m[K[36m[K:[m[Kelli è ser Branca Doria, e son più anni
[32m[K138[m[K[36m[K:[m[Kposcia passati ch’el fu sì racchiuso».
[32m[K139[m[K[36m[K:[m[K«Io credo», diss’ io lui, «che tu m’inganni;
[32m[K140[m[K[36m[K:[m[Kché Branca Doria[01;31m[K non [m[Kmorì unquanche,
[32m[K141[m[K[36m[K:[m[Ke mangia e bee e dorme e veste panni».
[32m[K142[m[K[36m[K:[m[K«Nel fosso sù», diss’ el, «de’ Malebranche,
[32m[K143[m[K[36m[K:[m[Klà dove bolle la tenace pece,
[32m[K144[m[K[36m[K:[m[Knon era ancora giunto Michel Zanche,
[32m[K145[m[K[36m[K:[m[Kche questi lasciò il diavolo in sua vece
[32m[K146[m[K[36m[K:[m[Knel corpo suo, ed un[01;31m[K suo [m[Kprossimano
[32m[K147[m[K[36m[K:[m[Kche ’l tradimento insieme con lui fece.
[32m[K148[m[K[36m[K:[m[KMa distendi oggimai in qua la mano;
[32m[K149[m[K[36m[K:[m[Kaprimi li occhi». E[01;31m[K io [m[Knon gliel’ apersi;
[32m[K150[m[K[36m[K:[m[Ke cortesia fu lui esser villano.
[32m[K151[m[K[36m[K:[m[KAhi Genovesi, uomini diversi
[32m[K152[m[K[36m[K:[m[Kd’ogne costume e pien d’ogne magagna,
[32m[K153[m[K[36m[K:[m[Kperché[01;31m[K non [m[Ksiete voi del mondo spersi?
[32m[K154[m[K[36m[K:[m[KChé col peggiore spirto di Romagna
[32m[K155[m[K[36m[K:[m[Ktrovai di voi un tal, che per sua opra
[32m[K156[m[K[36m[K:[m[Kin anima in Cocito già si bagna,
[32m[K157[m[K[36m[K:[m[Ke in corpo par vivo ancor di sopra.
