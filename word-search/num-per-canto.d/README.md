# Number of words per canto

This folder contains text files specific to a single word. Each file gives the number of occurrences of that word in each canto in a two-column format.

Use these files to quickly find the occurrences of a word in a specific canto.
